#!/bin/python3
import numpy as np
import matplotlib.pyplot as plt
n = 5
skprow = 9
length = 100
datfile = input('Enter data file: ')
data = np.loadtxt(datfile, skiprows=skprow, max_rows=n*(length+2))
x = data[0:length-1,0]
y = []
for i in range(n):
    idx = i * length
    y.append(data[idx:length+idx-1,1])
    print(y[i])
    print('length y: ', len(y[i]))
    plt.plot(x,y[i], label='column '+str(i))

plt.legend()
plt.show()
