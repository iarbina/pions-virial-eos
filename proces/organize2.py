#!/bin/python3
import numpy as np
import matplotlib.pyplot as plt
n = 5
skprow = 9
length = 100
dataS11 = np.loadtxt('S11_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))
dataS31 = np.loadtxt('S31_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))
dataP11 = np.loadtxt('P11_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))
dataP13 = np.loadtxt('P13_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))
dataP31 = np.loadtxt('P31_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))
dataP33 = np.loadtxt('P33_phase_band.gp', skiprows=skprow, max_rows=n*(length+2))

yS11 = dataS11[0:length-1,1]
yS31 = dataS31[0:length-1,1]
yP11 = dataP11[0:length-1,1]
yP13 = dataP13[0:length-1,1]
yP31 = dataP31[0:length-1,1]
yP33 = dataP33[0:length-1,1]

y = [yS11, \
     yS31, \
     yP11, \
     yP13, \
     yP31, \
     yP33]

lab = [r'$\delta^{1/2}_{0}$',  \
       r'$\delta^{3/2}_{0}$',  \
       r'$\delta^{1/2}_{1-}$', \
       r'$\delta^{1/2}_{1+}$', \
       r'$\delta^{3/2}_{1-}$', \
       r'$\delta^{3/2}_{1+}$']

# Data analysis
deg2rad = np.pi/180.0
mnuc = 939.0
mpi  = 139.6
M = mnuc + mpi
x = dataP11[0:length-1,0]

xi_orig = 1.1
xf_orig = 1.35
xi_noor = 50.1406
xf_noor = 212.508
Dx_orig = xf_orig - xi_orig
Dx_noor = xf_noor - xi_noor
x_orig  = Dx_orig/Dx_noor * (x - xi_noor) + xi_orig

yi_orig = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
yf_orig = np.array([15.0, -25.0, 40.0, -5.0, -10.0, 150.0])
yi_noor = np.array([9.42188, 146.785, 28.9844, 146.785, 159.785, 9.42188])
yf_noor = np.array([138.191, 14.6992, 153.871, 9.42188, 45.3203, 144.098])

Dy_orig = yf_orig - yi_orig
Dy_noor = yf_noor - yi_noor

y_orig = []
for i in range(6):
    y_orig.append(Dy_orig[i]/Dy_noor[i] * (y[i] - yi_noor[i]) + yi_orig[i])
    y_orig[i] = y_orig[i] * deg2rad

y_orig = np.asarray(y_orig)
x_orig = x_orig*1e3 #- M
# End of data analysis

# Plot
plt.rc('text', usetex=True)
font = {'family':'serif','size':16}
plt.rc('font', **font)
plt.tick_params(which='both',direction='in',top=True,right=True)

for i in range(6):
    plt.plot(x_orig, y_orig[i], label=lab[i])


#with open('phase_shifts.dat', 'w') as f:
#    for i in range(len(x_orig)):
#        f.writelines([x_orig[i], y_orig[0][i], y_orig[1][i], y_orig[2][i], y_orig[3][i], y_orig[4][i], y_orig[5][i]])

for i in range(len(x_orig)):
    print('{:.8f}  {:.8f}  {:.8f}  {:.8f}  {:.8f}  {:.8f}  {:.8f}'.format(x_orig[i], y_orig[0][i], y_orig[1][i], y_orig[2][i], y_orig[3][i], y_orig[4][i], y_orig[5][i]))

plt.axis([0, 300, -0.5, 3.0])
plt.xlabel(r'$E_\mathrm{cm} - M$ (MeV)')
plt.ylabel(r'Phase shift (rad)')
plt.tight_layout()
plt.legend(frameon=False, fontsize=12)
plt.show()
