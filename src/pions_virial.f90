subroutine pions_virial (rho, temp, ye_in, ymu_in,  &
    pions, muons, pi_interact, betaequi_tf,  &
    b2, nden, pres, ener, chem, sent, flags)
!
!
  use,intrinsic::iso_fortran_env,dp=>real64
  use fgsl
  use constants
  use defs_mod
  use bareos_mod, only: chembar, etabar, entrobar, abundbar_binc
  use lepeos_mod, only: abundlep, preslep, enerlep
  use boseos_mod
  use pieos_mod,  only: abundpi, pinucint, prespi, enerpi, pinucint_eta 
  use skyrme_mod, only: n0, skyrme_model
  use root_finder_mod
  implicit none
  real(dp), intent(in)  :: rho, temp, ye_in, ymu_in
  logical,  intent(in)  :: pions, muons, pi_interact, betaequi_tf
  real(dp), intent(out) :: b2(4)
  type(particle_density),    intent(out) :: nden
  type(generic_all),         intent(out) :: pres, ener, sent
  type(chemical_potentials), intent(out) :: chem
  type(flag_types),          intent(out) :: flags
  real(dp) :: nb, nguess
  real(dp) :: b2npi, b2ppi, db2npi, db2ppi
  real(dp) :: zn, zp, zpi, sbar
  real(dp) :: yn_in, yp_in
  real(dp) :: nn_in, np_in, ne_in, nmu_in
  real(dp) :: charge_neut_test, beta_equi_test
  real(dp) :: smeff(2), supot(2), spres, sedens
  real(dp), allocatable :: mroot(:), nparray(:)
  type(nucleons) :: eta
  type(nucleons) :: upot, meff
  integer :: istat, iounit, iter
  character(len=3) :: inp_path = 'inp'
  ! internal variables
  real(dp) :: ti, tf

  interface
    subroutine second_virial_coef (temp, b2npi, b2ppi, db2npi, db2ppi)
      use,intrinsic::iso_c_binding,dp=>c_double
      real(dp), intent(in) :: temp
      real(dp), intent(out) :: b2npi, b2ppi, db2npi, db2ppi
    end subroutine second_virial_coef
  end interface

  yp_in = ye_in + ymu_in
  yn_in = 1.0_dp - yp_in

  nb = rho/amu * 1.0e-39_dp
  nden%nb = nb
  nn_in  = yn_in  * nb
  np_in  = yp_in  * nb
  ne_in  = ye_in  * nb
  nmu_in = ymu_in * nb

  nparray = [0.001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9] * nb


!+---------------------------------------------------------------------+
! > compute the second Virial coefficient for given temperature        |
!+---------------------------------------------------------------------+
  if (pions .and. pi_interact) then
     !$OMP CRITICAL
     call second_virial_coef(temp, b2npi, b2ppi, db2npi, db2ppi)
     !$OMP END CRITICAL 
  else
     b2npi  = 0.0_dp
     b2ppi  = 0.0_dp
     db2npi = 0.0_dp
     db2ppi = 0.0_dp
  end if
  
  b2 = [b2npi, b2ppi, db2npi, db2ppi]
 
  
  ! change to nuclear standard units
  nguess = ye_in*nb

!+---------------------------------------------------------------------+
! > to compute the beta equilibrium                                    |
!+---------------------------------------------------------------------+
  if (betaequi_tf) then
    
    if (.not. pions) then
       ! call to the beta-equilibrium routine
       call betaequi (nb, temp, nguess, muons, [b2npi, b2ppi], chem%hat, nden%p, istat)
       
       call etabar (nb, temp, nden%p, eta%n, eta%p, istat)
       call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
       
       chem%n = temp * eta%n + mnmev + upot%n
       chem%p = temp * eta%p + mpmev + upot%p

       chem%hat  = chem%n - chem%p 
       chem%e  = chem%hat 
       chem%mu = chem%hat
    else if (pions .and. pi_interact) then
       iter = -1
       update_loop: do
         iter = iter + 1
         !print'(":: DEBUG:: iter,  yp = ",2(1x,g0))', iter, nden%p/nb
         if (iter > size(nparray)) exit
         if (allocated(mroot)) deallocate(mroot)
         
         call betaequipi (nb, temp, nguess, muons, [b2npi, b2ppi], eta, nden%p, istat)
         
         ! get baryon energy density and pressure from 'skyrme_mod' module 
         call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
         chem%n = temp * eta%n + mnmev + upot%n
         chem%p = temp * eta%p + mpmev + upot%p
         
         chem%hat  = chem%n - chem%p 
         chem%e  = chem%hat 
         chem%mu = chem%hat
         chem%pi = chem%hat
         !> check root finder status
         if (istat == 0) then
            exit
         else
            nguess = nparray(iter)
            cycle
         end if
         !<
       end do update_loop

    else if (pions .and. .not.pi_interact) then

    end if
    
    if (istat /=  0) then
       write(error_unit,'("pions_virial: betaequi returns error ",i2)') istat
       !stop 'Program halted!'
    end if
  
!+---------------------------------------------------------------------+
! > compute without beta equilibrium                                   |
!+---------------------------------------------------------------------+
  else if (.not.betaequi_tf) then

    nden%p = np_in
  
    ! for neutrons and protons
    call etabar (nb, temp, nden%p, eta%n, eta%p, istat)
    call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
    
    chem%n = temp * eta%n + mnmev + upot%n
    chem%p = temp * eta%p + mpmev + upot%p
    chem%hat = chem%n - chem%p
    
    ! for electrons
    if (ne_in/nb > 1.0e-9_dp) then
       if (allocated(mroot)) deallocate(mroot)
       call root_newton ((chem%n-chem%p-memev)/temp, newton_lepton,  &
                         [memev, temp, ne_in], mroot, istat)
       chem%e = temp * mroot(1) + memev
       if (allocated(mroot)) deallocate(mroot)
    else
       call set_to_zero_e
    end if

    ! for muons
    if (muons) then
       if (allocated(mroot)) deallocate(mroot)
       call root_newton ((chem%n-chem%p-mmumev)/temp, newton_lepton,  &
            [mmumev, temp, nmu_in], mroot, istat)
       chem%mu = temp * mroot(1) + mmumev
       if (allocated(mroot)) deallocate(mroot)
    else
       call set_to_zero_mu
    end if

    !+-----------------------------------------------------------------+
    ! > for interacting pions                                          |
    !+-----------------------------------------------------------------+
    if (pions .and. pi_interact) then
      
       iter = -1
       test_loop: do
         iter = iter + 1
         !print'(":: DEBUG:: iter,  yp = ",2(1x,g0))', iter, nden%p/nb
         if (iter > size(nparray)) exit
         if (allocated(mroot)) deallocate(mroot)
         

         call fgsl_multiroot (3, [eta%n, eta%p, nden%p], fsvc_pi_3d_binc, &
              [nb, temp, ne_in, nmu_in, b2npi, b2ppi], mroot, istat)
         
         eta%n  = mroot(1)
         eta%p  = mroot(2)
         nden%p = mroot(3)
         if (allocated(mroot)) deallocate(mroot)
        
         !> check root finder status
         if (istat == 0) then
            exit
         else
            nden%p = nparray(iter)
            call etabar (nb, temp, nden%p, eta%n, eta%p, istat)
            call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
            cycle
         end if
         !<
       end do test_loop
       
       ! call the Skryme model to update Upot values
       call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
       
       chem%n = temp * eta%n + mnmev + upot%n
       chem%p = temp * eta%p + mpmev + upot%p
    
       chem%hat = chem%n - chem%p
       chem%pi  = chem%hat
       
    !+-----------------------------------------------------------------+
    ! > for ideal Bose gas of pions                                    |
    !+-----------------------------------------------------------------+
    else if (pions .and. .not.pi_interact) then
      
      find_minmax: block
        real(dp) :: np_min, np_max
        
        ! > search for lowest np (np_min) such that mu_n - mu_p = m_pi
        if (allocated(mroot)) deallocate(mroot)
        call fgsl_root ([0.00001_dp*nb, 0.99999_dp*nb], chemeq_pi_binc, &
                        [1.0_dp], mroot, istat)
        
        if (istat == fgsl_einval) then
           np_min = 0.00001_dp*nb
        else
           if(allocated(mroot)) np_min = mroot(1)
        end if
        
        ! > search for lowest np (np_max) such that mu_n - mu_p = - m_pi
        if (allocated(mroot)) deallocate(mroot)
        call fgsl_root ([0.00001_dp*nb, 0.99999_dp*nb], chemeq_pi_binc, &
                        [-1.0_dp], mroot, istat)
        
        if (istat == fgsl_einval) then
           np_max = 0.99999_dp*nb
        else
           if(allocated(mroot)) np_max = mroot(1)
        end if
      
        ! > search for np such that np = nlep + npi [+ npi_cond]
        ! > with the condition mu_n - mu_p = mu_pi and
        ! > withing the interval (np_min, nb)
        if (allocated(mroot)) deallocate(mroot)
        call fgsl_root ([np_min, np_max], fbose_pi_1d_binc, &
                        [nb, temp, ne_in, nmu_in], mroot, istat)
        
        if (istat == fgsl_einval) then
           nden%p = np_min
           call abundbos (mpimev, temp, mpimev, &
                nden%bpi%net, nden%bpi%pim, nden%bpi%pip)
           nden%bpi%pic = nden%p - ne_in - nmu_in - nden%bpi%net
        else
           if(allocated(mroot)) nden%p = mroot(1)
           nden%bpi%pic = 0.0_dp
        end if
        
        if (allocated(mroot)) deallocate(mroot)
      end block find_minmax
      
      call etabar (nb, temp, nden%p, eta%n, eta%p, istat)
      call skyrme_model (nb, nden%p, temp, eta, meff, upot, spres, sedens)
      
      chem%n = temp * eta%n + mnmev + upot%n
      chem%p = temp * eta%p + mpmev + upot%p
      chem%hat = chem%n - chem%p
      chem%pi  = chem%hat
       
    end if

    flags%froot%int_val = istat

    if (istat /=  0) then
       flags%froot%log_val = .false.
       write(error_unit,'("WARNING: pions_virial: returns error ",i2)') istat
     else
       flags%froot%log_val = .true.
    end if

  end if

!+---------------------------------------------------------------------+
! > re-compute thermodynamical quantities for the root solutions       |
!+---------------------------------------------------------------------+

  ! baryons quantities
  nden%n = nb - nden%p
  pres%bar = spres
  ener%bar = sedens
  sent%bar = entrobar (nb, temp, nden%p, [eta%n, eta%p])
  
  ! for the electrons
  if (ne_in/nb > 1.0e-9_dp) then
     call abundlep (memev, temp, chem%e, nden%e%net, nden%e%par, nden%e%ant)
     call preslep  (memev, temp, chem%e, pres%e%net, pres%e%par, pres%e%ant)
     call enerlep  (memev, temp, chem%e, ener%e%net, ener%e%par, ener%e%ant)
     sent%e%par = (pres%e%par + ener%e%par - chem%e * nden%e%par) / temp / nb
     sent%e%ant = (pres%e%ant + ener%e%ant + chem%e * nden%e%ant) / temp / nb
     sent%e%net = sent%e%par + ener%e%ant
  else
     call set_to_zero_e
  end if
  
  ! for the muons
  if (muons) then
     call abundlep (mmumev, temp, chem%mu, nden%mu%net, nden%mu%par, nden%mu%ant)
     call preslep  (mmumev, temp, chem%mu, pres%mu%net, pres%mu%par, pres%mu%ant)
     call enerlep  (mmumev, temp, chem%mu, ener%mu%net, ener%mu%par, ener%mu%ant)
     sent%mu%par = (pres%mu%par + ener%mu%par - chem%mu * nden%mu%par) / temp / nb
     sent%mu%ant = (pres%mu%ant + ener%mu%ant + chem%mu * nden%mu%ant) / temp / nb
     sent%mu%net = sent%mu%par + ener%mu%ant
  else
     call set_to_zero_mu
  end if
  
  ! fugacities
  zn  = exp((chem%n-mnmev)/temp)
  zp  = exp((chem%p-mpmev)/temp)
  if (pions) then
     zpi = exp((chem%pi-mpimev)/temp)
  else
     zpi = 0.0_dp
  end if


  ! for pions
  if (pions .and. pi_interact) then
    
     !> interacting pions (second virial coefficient)
     nden%pi%pim = abundpi (temp, chem%pi)
     !we sum the interaccion contribution
     nden%pi%npi = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2npi)
     nden%pi%ppi = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2ppi)
     nden%pi%tot = nden%pi%pim + nden%pi%npi + nden%pi%ppi
     
     ! pions pressure
     pres%pi%pim = prespi (temp, nden%pi%pim)
     ! add the interaction part
     pres%pi%npi = temp * nden%pi%npi
     pres%pi%ppi = temp * nden%pi%ppi
     pres%pi%tot = pres%pi%pim + temp * (nden%pi%npi + nden%pi%ppi)
     
     ! pions internal energy
     ener%pi%pim = enerpi (temp, chem%pi)
     ! add the interaction part
     ener%pi%npi = zpi * zn * db2npi
     ener%pi%ppi = zpi * zn * db2ppi
     ener%pi%tot = ener%pi%pim + zpi * (zn*db2npi + zp*db2ppi)
     
     sent%pi%tot = (pres%pi%tot + ener%pi%tot - chem%pi * nden%pi%tot) / temp / nb

     call set_to_zero_bpi
  else if (pions .and. .not.pi_interact) then
     
     !> non-interacting pions (Bose gas)
     call abundbos (mpimev, temp, chem%pi, &
          nden%bpi%net, nden%bpi%pim, nden%bpi%pip, mpi0mev, nden%bpi%pi0)
     
     call presbos (mpimev, temp, chem%pi, &
          pres%bpi%net, pres%bpi%pim, pres%bpi%pip, mpi0mev, pres%bpi%pi0)

     call enerbos (mpimev, temp, chem%pi, &
          ener%bpi%net, ener%bpi%pim, ener%bpi%pip, mpi0mev, ener%bpi%pi0)

     sent%bpi%pim = (pres%bpi%pim + ener%bpi%pim - chem%pi * nden%bpi%pim) / temp / nb
     sent%bpi%pip = (pres%bpi%pip + ener%bpi%pip + chem%pi * nden%bpi%pip) / temp / nb
     sent%bpi%net =  sent%bpi%pim + ener%bpi%pip
     sent%bpi%pi0 = (pres%bpi%pi0 + ener%bpi%pi0) / temp / nb

     call set_to_zero_pi
  else
     call set_to_zero_pi
     call set_to_zero_bpi
  end if

  ! > radiation
  radiation: block
    real(dp), parameter :: ssb = pi*pi/(60.0_dp*hbarcfm**3)
    ener%rad = 4.0_dp * ssb * temp**4
    pres%rad = ener%rad / 3.0_dp
    sent%rad = 4.0_dp * pres%rad / temp
  end block radiation

  ! set total quantities
  if (pions .and. pi_interact) then
     pres%tot = pres%bar + pres%e%net + pres%mu%net + pres%rad + pres%pi%tot
     ener%tot = ener%bar + ener%e%net + ener%mu%net + ener%rad + ener%pi%tot  &
              + nden%n * mnmev + nden%p * mpmev
     sent%tot = sent%bar + sent%e%net + sent%mu%net + sent%rad + sent%pi%tot
  else if (pions .and. .not.pi_interact) then
     pres%tot = pres%bar + pres%e%net + pres%mu%net + pres%rad + pres%bpi%net + pres%bpi%pi0
     ener%tot = ener%bar + ener%e%net + ener%mu%net + ener%rad + ener%bpi%net + ener%bpi%pi0  &
              + nden%n * mnmev + nden%p * mpmev
     sent%tot = sent%bar + sent%e%net + sent%mu%net + sent%rad + sent%bpi%net + sent%bpi%pi0
  else
     pres%tot = pres%bar + pres%e%net + pres%mu%net + pres%rad 
     ener%tot = ener%bar + ener%e%net + ener%mu%net + ener%rad   &
              + nden%n * mnmev + nden%p * mpmev
     sent%tot = sent%bar + sent%e%net + sent%mu%net + sent%rad 
  end if

!+---------------------------------------------------------------------+
!  > test the results:                                                 |
!    - beta equlibrium: if true                                        |
!    - charge neutrality: always                                       |
!    - validity if SVC approximation: if zn & zp & zpi < 0.5           |
!+---------------------------------------------------------------------+

  call test_beta_equilibrium
  call test_charge_neutrality
  call test_svc_expansion

contains

  function chemeq_pi_binc (x, params) bind(c)
    use, intrinsic :: iso_c_binding
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double)     :: chemeq_pi_binc
    real(fgsl_double), pointer :: par(:)
    ! internal variables
    real(dp) :: np

    np = x
    
    call c_f_pointer(params, par, (/ 1 /))

    call etabar (nb, temp, np, eta%n, eta%p, istat)
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    
    chem%n = temp * eta%n + mnmev + upot%n
    chem%p = temp * eta%p + mpmev + upot%p

    chemeq_pi_binc = chem%n - chem%p - par(1) * mpimev
  end function chemeq_pi_binc
   
  subroutine test_beta_equilibrium
    ! cotton prove: test beta equilibrium
    beta_equi_test = abs(chem%n - chem%p - chem%e)
    flags%betae%re_val = beta_equi_test
    flags%betae%log_val = betaequi_tf
    
    if ( beta_equi_test  < 1.0d-10 ) then
       flags%betae%int_val = 0
    else
       flags%betae%int_val = 1
    end if
  end subroutine test_beta_equilibrium
  
  subroutine test_charge_neutrality
    ! cotton prove: test charge neutrality
    if (pi_interact) &
        charge_neut_test = abs(nden%p - nden%e%net - nden%mu%net - nden%pi%tot) / nb
    if (.not.pi_interact)   &
        charge_neut_test =  &
        abs(nden%p - nden%e%net - nden%mu%net - nden%bpi%net - nden%bpi%pic ) / nb
    flags%charg%re_val = charge_neut_test

    if ( charge_neut_test < 1.0d-4 ) then
       flags%charg%int_val = 0
       flags%charg%log_val = .true.
    else
       flags%charg%int_val = 1
       flags%charg%log_val = .false.
    end if
  end subroutine test_charge_neutrality

  subroutine test_svc_expansion
    ! validity of second virial coefficient approximation
    flags%virial%re_val = zn + zp + zpi
    if (zn < 0.5_dp .and. zp < 0.5_dp .and. zpi < 0.5_dp) then
       flags%virial%int_val = 0
       flags%virial%log_val = .true.
    else
       flags%virial%int_val = 1
       flags%virial%log_val = .false.
    end if
  end subroutine test_svc_expansion
  
  subroutine set_to_zero_e
     chem%e  = 0.0_dp
     nden%e%net = 0.0_dp; nden%e%par = 0.0_dp; nden%e%ant = 0.0_dp
     pres%e%net = 0.0_dp; pres%e%par = 0.0_dp; pres%e%ant = 0.0_dp
     ener%e%net = 0.0_dp; ener%e%par = 0.0_dp; ener%e%ant = 0.0_dp
     sent%e%net = 0.0_dp; sent%e%par = 0.0_dp; sent%e%ant = 0.0_dp
  end subroutine set_to_zero_e
  
  subroutine set_to_zero_mu
     chem%mu  = 0.0_dp
     nden%mu%net = 0.0_dp; nden%mu%par = 0.0_dp; nden%mu%ant = 0.0_dp
     pres%mu%net = 0.0_dp; pres%mu%par = 0.0_dp; pres%mu%ant = 0.0_dp
     ener%mu%net = 0.0_dp; ener%mu%par = 0.0_dp; ener%mu%ant = 0.0_dp
     sent%mu%net = 0.0_dp; sent%mu%par = 0.0_dp; sent%mu%ant = 0.0_dp
  end subroutine set_to_zero_mu

  subroutine set_to_zero_pi
     nden%pi%pim = 0.0_dp; nden%pi%npi = 0.0_dp; nden%pi%ppi = 0.0_dp; nden%pi%tot = 0.0_dp
     pres%pi%pim = 0.0_dp; pres%pi%npi = 0.0_dp; pres%pi%ppi = 0.0_dp; pres%pi%tot = 0.0_dp
     ener%pi%pim = 0.0_dp; ener%pi%npi = 0.0_dp; ener%pi%ppi = 0.0_dp; ener%pi%tot = 0.0_dp
     sent%pi%pim = 0.0_dp; sent%pi%npi = 0.0_dp; sent%pi%ppi = 0.0_dp; sent%pi%tot = 0.0_dp
  end subroutine set_to_zero_pi
  
  subroutine set_to_zero_bpi
     nden%bpi%pim = 0.0_dp; nden%bpi%pip = 0.0_dp; nden%bpi%pic = 0.0_dp; nden%bpi%net = 0.0_dp
     pres%bpi%pim = 0.0_dp; pres%bpi%pip = 0.0_dp; pres%bpi%pic = 0.0_dp; pres%bpi%net = 0.0_dp
     ener%bpi%pim = 0.0_dp; ener%bpi%pip = 0.0_dp; ener%bpi%pic = 0.0_dp; ener%bpi%net = 0.0_dp
     sent%bpi%pim = 0.0_dp; sent%bpi%pip = 0.0_dp; sent%bpi%pic = 0.0_dp; sent%bpi%net = 0.0_dp
     nden%bpi%pi0 = 0.0_dp; pres%bpi%pi0 = 0.0_dp; ener%bpi%pi0 = 0.0_dp; sent%bpi%pi0 = 0.0_dp
  end subroutine set_to_zero_bpi
  
end subroutine pions_virial
