module bareos_mod
  use, intrinsic::iso_fortran_env, dp=>real64
  use fgsl
  use defs_mod
  use constants
  use skyrme_mod
  use fermi_integrals_mod
  implicit none
  private
  public :: chembar, etabar, abundbar, nnuc, entrobar, &
            abundbar_binc
  real(dp) :: nnuc(2)
contains

  subroutine chembar (nb, temp, np, mun, mup, istat)
    use, intrinsic :: iso_c_binding
    implicit none
    real(dp), intent(in) :: nb, temp, np
    real(dp), intent(out) :: mun, mup
    integer,  intent(out) :: istat
    real(dp) :: spres, sedens
    type(nucleons) :: meff, upot, eta


    istat = 0
    if ( nb < np ) then
       istat = 2
       write(output_unit,'("bareos: WARNING: np > nb, returning...")')
       !return
    end if

    call etabar (nb, temp, np, eta%n, eta%p, istat)    

    ! with this eta, compute the Skyrme quantities
    call skyrme_model (nb, nnuc(2), temp, eta, meff, upot, spres, sedens)

    ! compute the chemical potential: mu = T*eta + m + U
    ! U = supot from 'skyrme_mod' module
    mun = temp*eta%n + mnmev + upot%n
    mup = temp*eta%p + mpmev + upot%p
  end subroutine chembar

  subroutine etabar (nb, temp, np, etan, etap, istat)
    use, intrinsic :: iso_c_binding
    implicit none
    real(dp), intent(in) :: nb, temp, np
    real(dp), intent(out) :: etan, etap
    integer,  intent(out) :: istat
    real(dp) :: eta(2), arg(2), fac, smeff(2)

    istat = 0
    
    ! compute number densities: baryon, neutron and proton
    !nb = rho/amu * 1.0e-39_dp
    nnuc(2) = np
    nnuc(1) = nb - nnuc(2)
#if (DEBUG > 1)
    write(output_unit,'("bareso_mod: DEBUG: nnuc:",2(1x,g0))') nnuc
#endif

    smeff(1) = skyrme_meff (mnmev, nnuc(1), nb)
    smeff(2) = skyrme_meff (mpmev, nnuc(2), nb)
#if (DEBUG > 1)
    write(output_unit,'("bareso_mod: DEBUG: smeff:",2(1x,g0))') smeff
#endif

    ! compute eta by inverting Fermi function
    fac = 2.0_dp*temp
    fac = hbarcfm*hbarcfm/fac
    fac = fac*fac*fac
    fac = 2.0_dp*pi*pi*sqrt(fac)
    
    ! arg = 2*pi**2*nnuc*(hbarc**2/2*meff*T)**(3/2)
    arg(1) = fac*nnuc(1)/(sqrt(smeff(1)**3))
    arg(2) = fac*nnuc(2)/(sqrt(smeff(2)**3))
#if (DEBUG > 1)
    write(output_unit,'("bareso_mod: DEBUG: arg:",2(1x,g0))') arg
#endif

    eta(1) = inverse_fermi_one_half(arg(1))
    eta(2) = inverse_fermi_one_half(arg(2))
#if (DEBUG > 1)
    write(output_unit,'("bareso_mod: DEBUG: eta:",2(1x,g0))') eta
#endif
    etan = eta(1)
    etap = eta(2)
  end subroutine etabar

  function abundbar (nb, temp, nx, eta, mass)
    implicit none
    real(dp) :: nb, temp, nx, eta, mass, abundbar
    real(dp) :: fact, meff
    
    meff = skyrme_meff (mass, nx, nb)

! > If Fukushima routines are used
    fact = hbarcfm * hbarcfm
    fact = 2.0_dp * meff * temp / fact
    fact = 0.5_dp * sqrt(fact*fact*fact) / (pi*pi)
    
    abundbar = fact * fermi_one_half(eta)
  end function abundbar

  function abundbar_binc (x, params) bind(c)
    use, intrinsic :: iso_c_binding
    use fgsl
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double) :: abundbar_binc
    real(fgsl_double), pointer :: par(:)
    !
    real(dp) :: nb, temp, nx, mass, meffx, etax
    real(dp) :: fact

    nx = x
    
    call c_f_pointer(params, par, (/ 3 /))
    nb   = par(1)
    temp = par(2)
    mass = par(3)
    etax = par(4)
    
    meffx = skyrme_meff (mass,  nx, nb)
    
    fact = hbarcfm * hbarcfm
    fact = 2.0_dp * meffx * temp / fact
    fact = 0.5_dp * sqrt(fact*fact*fact) / (pi*pi)
    
    abundbar_binc = (nx - fact * fermi_one_half(etax)) 
  end function abundbar_binc

  function entrobar (nb, temp, np, eta)
    ! kinn and kinp variables come from 'skyrme_mod' module from the use statement
    implicit none
    real(dp) :: nb, temp, np, nn, smeff(2), eta(2), entrobar

    nn = nb - np
    smeff(1) = skyrme_meff (mnmev, nnuc(1), nb)
    smeff(2) = skyrme_meff (mpmev, nnuc(2), nb)
    
    entrobar = 5.0_dp*hbarcmev**2 * (kinn/smeff(1) + kinp/smeff(2)) / (6.0_dp*temp)
    entrobar = entrobar - nn * eta(1) - np * eta(2)
    entrobar = entrobar / nb
  end function entrobar

!  function abundbarpi (inuc, nb, temp, np, etan, etap, b2)
!    implicit none
!    integer  :: inuc
!    real(dp) :: nb, temp, np, etan, etap, b2
!    real(dp) :: arg, delta
!    type upot
!      real(dp) :: n, p
!    end type upot
!
!    delta = mnmev - mpmev
!
!    call skyrme_model (nb, np, temp, [etan, etap])
!    upot%n = supot(1)
!    upot%p = supot(2)
!
!    if (inuc == 1) then
!       arg = (2.0_dp * upot%n - upot%p + delta - mpimev) /temp
!       arg = 2.0_dp*etan - etap + arg
!    else if (inuc == 2) then
!       arg = (upot%n + delta - mpimev) / temp
!       arg = etan + arg
!    end if
!
!    abundbarpi = exp(arg) * b2
!  end function abundbarpi

end module bareos_mod
