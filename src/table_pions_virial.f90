program table_pions_virial
!
!>  ne(:): abundance of electrons
!       0: total = ne(1) + ne(2)
!       1: particle
!       2: anti-particle
!> nmu(:): abundance of electrons
!       0: total = nmu(1) + nmu(2)
!       1: particle
!       2: anti-particle
!>   npie: abundance of pions in excited states
!>   npic: abundance of pions in the condensate
!>   npim: abundance of positive pions = npie + npic
!>   nnpi: abundance of pios due to interaction with neutron
!>   nppi: abundance of pios due to interaction with protons
!
  use,intrinsic::iso_fortran_env,dp=>real64
  use  ieee_arithmetic
#ifdef _OPENMP
  use omp_lib
#endif
  use defs_mod
  use constants
  implicit none
  real(dp) :: b2(4)
  real(dp) :: zn, zp, zpi
  type(particle_density)    :: nden
  type(generic_all)         :: pres, ener, sent
  type(chemical_potentials) :: chem
  type(flag_types) :: flags
  logical :: pions, muons, pi_interact, betaequi_tf, output_file, external_call
  integer :: iounit
  character(len=3) :: inp_path = 'inp'
  ! table variables
  real(dp), parameter :: amumev = 931.49410242_dp
  integer :: i, j, k
  integer  :: nrho, ntemp, nye, num_threads
  real(dp) :: rho_max, rho_min, temp_max, temp_min, ye_max, ye_min
  real(dp), allocatable, dimension(:) :: rhoarray, tarray, yearray
  real(dp), allocatable, dimension(:,:,:) :: parray, earray, muearray, muparray  
  real(dp), allocatable, dimension(:,:,:) :: munarray, dummy, garray, sarray
  real(dp), allocatable, dimension(:,:,:) :: yp, ye, ymu, ypi
  real(dp), allocatable, dimension(:,:,:) :: pbar, pe, pmu, ppi
  real(dp), allocatable, dimension(:,:,:) :: ebar, ee, emu, epi
  real(dp), allocatable, dimension(:,:,:) :: sbar, se, smu, spi
  integer,  allocatable, dimension(:,:,:) :: svc_flag ! validity second virial coef flag (0 = good, 1 = bad)
  character(len=*), parameter  :: filename = 'table.bin'
  character(len=:), allocatable :: tabname 
  character(len=1024) :: date, time, cpus, content

  !--------------------------------------------------------------------
  ! read namelist
  !--------------------------------------------------------------------
  namelist /control/ muons, pions, pi_interact, betaequi_tf,  &
                     output_file, external_call
  namelist /table/ nrho, ntemp, nye,    &
                   rho_max, rho_min,    &
                   temp_max, temp_min,  &
                   ye_max, ye_min

  ! open and read 'control' namelist
  open(newunit=iounit, file=inp_path//'/input_list.nml', status='old', &
           action='read', form='formatted', access='sequential')
  read(iounit, nml=control)
  close(iounit)
  
  ! open and read 'table' namelist
  open(newunit=iounit, file=inp_path//'/input_list.nml', status='old', &
                action='read', form='formatted', access='sequential')
  read(iounit, nml=table)
  close(iounit)

  !-------------------------------------------------------------------- 
  ! allocate table params and outputs
  !--------------------------------------------------------------------
  call date_and_time (date=date, time=time)
#ifdef _OPENMP
  write(cpus,'(i0.2)') OMP_GET_MAX_THREADS()
#else
  cpus = '01'
#endif
  content = 'npe'
  if (muons) content = trim(content)//'mu'
  if (pions) content = trim(content)//'pi'

  allocate( rhoarray(nrho), tarray(ntemp), yearray(nye) )
  allocate( parray(nrho, ntemp, nye),   & 
            earray(nrho, ntemp, nye),   &
            muearray(nrho, ntemp, nye), &
            muparray(nrho, ntemp, nye), &
            munarray(nrho, ntemp, nye), &
            dummy(nrho, ntemp, nye),    &
            garray(nrho, ntemp, nye),   &
            sarray(nrho, ntemp, nye),   &
            svc_flag(nrho, ntemp, nye) )
  allocate( yp(nrho, ntemp, nye),       & 
            ye(nrho, ntemp, nye),       & 
            ymu(nrho, ntemp, nye),      & 
            ypi(nrho, ntemp, nye),      & 
            pbar(nrho, ntemp, nye),     & 
            pe(nrho, ntemp, nye),       & 
            pmu(nrho, ntemp, nye),      & 
            ppi(nrho, ntemp, nye),      & 
            ebar(nrho, ntemp, nye),     & 
            ee(nrho, ntemp, nye),       & 
            emu(nrho, ntemp, nye),      & 
            epi(nrho, ntemp, nye),      & 
            sbar(nrho, ntemp, nye),     & 
            se(nrho, ntemp, nye),       & 
            smu(nrho, ntemp, nye),      & 
            spi(nrho, ntemp, nye) )
  
  ! table arrays
  if (ntemp < 2) then
    tarray(:) = temp_min
  else
    tarray   = [ (temp_min*10.0_dp**((i-1)*(log10(temp_max)-log10(temp_min))/(ntemp-1)), i=1, ntemp) ]
  end if
  if (nrho < 2) then
    rhoarray(:) = rho_min
  else
    rhoarray = [ (rho_min *10.0_dp**((i-1)*(log10(rho_max)-log10(rho_min))/(nrho-1)), i=1, nrho) ]
  end if
  if (nye < 2) then
    yearray(:) = ye_min
  else
    yearray  = [ (ye_min + (i-1)*(ye_max-ye_min)/(nye-1), i=1, nye) ]
  end if

  write(output_unit,'(":: Table params:")')
  write(output_unit,'("=> rho_min  = ",es10.3,", rho_max  = ",es10.3,", nrho  = ",i3)') rho_min, rho_max, nrho
  write(output_unit,'("=> temp_min = ",es10.3,", temp_max = ",es10.3,", ntemp = ",i3)') temp_min, temp_max, ntemp
  write(output_unit,'("=> ye_min   = ",es10.3,", ye_max   = ",es10.3,", nue   = ",i3)') ye_min, ye_max, nye
  
  !--------------------------------------------------------------------
  ! Loops for the 3D table (i: density, j: temperature, k: ye)
  !--------------------------------------------------------------------
  ! loop for electron abundaces
  !$OMP PARALLEL DO PRIVATE(b2, nden, pres, ener, chem, sent, flags)
  !!$OMP PARALLEL DO DEFAULT(SHARED)  &
  !!$OMP PRIVATE(b2, nden, pres, ener, chem, sent, flags)
  ye_loop: do k=1, nye

    ! loop for temperature
    temp_loop: do j=1, ntemp

      ! loop for densities
      rho_loop: do i=1, nrho      

#ifdef _OPENMP
        print'(" :: Number threads ",g0,", Thread number ",g0, " i:",i3,", j:",i3,", k:",i3,&
        & ", rho: ",es10.4,", T: ",es10.4,", Ye: ",es10.4,", CN: ",g0,", CNerr: ",g0)',     &
          OMP_GET_NUM_THREADS(), OMP_GET_THREAD_NUM(),        &
          i, j, k,                                &
          rhoarray(i), tarray(j), yearray(k)
#else
        print'(" :: i:",i3,", j:",i3,", k:",i3,&
        & ", rho: ",es10.4,", T: ",es10.4,", Ye: ",es10.4,", CN: ",g0,", CNerr: ",g0)',     &
          i, j, k,                                &
          rhoarray(i), tarray(j), yearray(k)
#endif

        call pions_virial (rhoarray(i), tarray(j), yearray(k), 0.0_dp,  &
          pions, muons, pi_interact, betaequi_tf,  &
          b2, nden, pres, ener, chem, sent, flags)
        
!       fugacities
        !zn  = exp((chem%n-mnmev)/tarray(j))
        !zp  = exp((chem%p-mpmev)/tarray(j))
        !zpi = exp((chem%pi-mpimev)/tarray(j))
        
        ! fill up table arrays
        !parray(i,j,k) = log10( pres%tot * 1.0e39_dp )
        !earray(i,j,k) = log10( ener%tot * 1.0e39_dp - rhoarray(i) * amumev / amu)
        parray(i,j,k) = pres%tot
        earray(i,j,k) = ener%tot
        
        muearray(i,j,k) = chem%e
        muparray(i,j,k) = chem%p
        munarray(i,j,k) = chem%n
        
        dummy(i,j,k)  = 0.0d0
        garray(i,j,k) = 0.0d0
        sarray(i,j,k) = sent%tot
        svc_flag(i,j,k) = flags%virial%int_val

        yp(i,j,k)  = nden%p/nden%nb
        ye(i,j,k)  = nden%e%net/nden%nb
        ymu(i,j,k) = nden%mu%net/nden%nb
        ypi(i,j,k) = nden%pi%tot/nden%nb

        pbar(i,j,k) = pres%bar
        pe(i,j,k)   = pres%e%net
        pmu(i,j,k)  = pres%mu%net
        ppi(i,j,k)  = pres%pi%tot

        ebar(i,j,k) = ener%bar
        ee(i,j,k)   = ener%e%net
        emu(i,j,k)  = ener%mu%net
        epi(i,j,k)  = ener%pi%tot
        
        sbar(i,j,k) = sent%bar
        se(i,j,k)   = sent%e%net
        smu(i,j,k)  = sent%mu%net
        spi(i,j,k)  = sent%pi%tot
        
        if ( ieee_is_nan(parray(i,j,k)).or.ieee_is_nan(earray(i,j,k)) )  &
          error stop "ERROR: table_pions_virial: NAN"
        
#ifdef _OPENMP
        print'("  > Number threads ",g0,", Thread number ",g0, " i:",i3,", j:",i3,", k:",i3,&
        & ", rho: ",es10.4,", T: ",es10.4,", Ye: ",es10.4,", CN: ",g0,", CNerr: ",g0)',     &
          OMP_GET_NUM_THREADS(), OMP_GET_THREAD_NUM(),        &
          i, j, k,                                &
          rhoarray(i), tarray(j), yearray(k),     &
          flags%charg%log_val, flags%charg%re_val
#else
        print'("  > i:",i3,", j:",i3,", k:",i3,&
        & ", rho: ",es10.4,", T: ",es10.4,", Ye: ",es10.4,", CN: ",g0,", CNerr: ",g0)',     &
          i, j, k,                                &
          rhoarray(i), tarray(j), yearray(k)
          rhoarray(i), tarray(j), yearray(k),     &
          flags%charg%log_val, flags%charg%re_val
#endif


        if (.not.flags%charg%log_val) then
          read(input_unit,*)
        end if

      end do rho_loop
    end do temp_loop
  end do ye_loop
  !$OMP END PARALLEL DO
  
  tabname = trim('table_'//trim(date)//'_'//trim(time)//'_'//&
    &trim(content)//'_'//trim(cpus)//'cpus')

  call write_hdf5_table
  !--------------------------------------------------------------------
  ! write arrays in file
  !--------------------------------------------------------------------
  open(newunit=iounit, file=trim(tabname//'.bin'),  &
    &  form='unformatted', action='write', convert='big_endian')
  write(iounit) nrho, ntemp, nye, log10(rhoarray), log10(tarray), yearray, parray, earray,  &
                muearray,muparray,munarray,dummy,garray,sarray,svc_flag
  close(iounit)

  write(output_unit,'("Program to create table EOS finished!")')

contains

  subroutine write_hdf5_table
    use hdf5
    implicit none
    integer(HID_T) :: file_id, dspace_id, dset_id
    integer(HSIZE_T), dimension(1) :: dime1
    integer(HSIZE_T), dimension(3) :: dime3
    integer :: error, rank
    character(len=:), allocatable :: h5tablename
    
    h5tablename = tabname//'.h5'
    
    dime3 = [nrho, ntemp, nye]

    ! initialize hdf5 interface and create new database
    call h5open_f(error)
    call h5fcreate_f(h5tablename, H5F_ACC_TRUNC_F, file_id, error)
    
    ! create new entry / save number of density points
    rank = 1; dime1(1) = 1
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'nrho', H5T_NATIVE_INTEGER, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, nrho, dime1, error)
    ! close entry
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save number of temperature points
    rank = 1; dime1(1) = 1
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'ntemp', H5T_NATIVE_INTEGER, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, ntemp, dime1, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save number of Ye points
    rank = 1; dime1(1) = 1
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'nye', H5T_NATIVE_INTEGER, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, nye, dime1, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save density
    rank = 1; dime1(1) = nrho
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'rho', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, rhoarray, dime1, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save tempurature
    rank = 1; dime1(1) = ntemp
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'temp', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, tarray, dime1, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save Ye
    rank = 1; dime1(1) = nye
    call h5screate_simple_f(rank, dime1, dspace_id, error)
    call h5dcreate_f(file_id, 'Y_e', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, yearray, dime1, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)
    
    ! save Yp
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'Y_p', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, yp, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save Ymu
    if (muons) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'Y_mu', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, ymu, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save Ypi
    if (pions .and. pi_interact) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'Y_pi', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, ypi, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save Pbar
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'P_bar', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, pbar, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save Pe
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'P_e', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, pe, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save pmu
    if (muons) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'P_mu', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, pmu, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save ppi
    if (pions .and. pi_interact) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'P_pi', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, ppi, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save ebar
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'u_bar', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, ebar, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save ee
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'u_e', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, ee, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save emu
    if (muons) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'u_mu', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, emu, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save epi
    if (pions .and. pi_interact) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 'u_pi', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, epi, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save sbar
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 's_bar', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, sbar, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save se
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 's_e', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, se, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save smu
    if (muons) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 's_mu', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, smu, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if

    ! save spi
    if (pions .and. pi_interact) then
       rank = 3
       call h5screate_simple_f(rank, dime3, dspace_id, error)
       call h5dcreate_f(file_id, 's_pi', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
       call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, spi, dime3, error)
       call h5dclose_f(dset_id, error)
       call h5sclose_f(dspace_id, error)
    end if


    ! save pressure
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'P_tot', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, parray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save energy density
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'E_tot', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, earray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save neutron chemical potential
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'mu_n', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, munarray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save proton chemical potential
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'mu_p', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, muparray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save electron chemical potential
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'mu_e', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, muearray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save charge chemical potential
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'mu_hat', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, munarray-muparray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save entropy
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 's_tot', H5T_NATIVE_DOUBLE, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, sarray, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)

    ! save svc flag
    rank = 3
    call h5screate_simple_f(rank, dime3, dspace_id, error)
    call h5dcreate_f(file_id, 'svc_flag', H5T_NATIVE_INTEGER, dspace_id, dset_id, error)
    call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, svc_flag, dime3, error)
    call h5dclose_f(dset_id, error)
    call h5sclose_f(dspace_id, error)


    ! close and finalize hdf5 database
    call h5fclose_f(file_id, error)
    call h5close_f(error)
    
  end subroutine write_hdf5_table

end program table_pions_virial
