module pass_roots
  use, intrinsic :: iso_fortran_env, dp=>real64
  implicit none
  private
  real(dp), public :: eroot1, eroot2
end module pass_roots

module root_finder_mod
  ! This module collects all (sub)routines devoted to find
  ! roots of functions. There's no intention to create
  ! commmon or shared variables between them, each of them
  ! thus create an independent unit
  implicit none
  private
  public :: betaequi, betaequipi, nobetaequi, &
            nobetaequi_brent, nobetaequipi,   &
            root_newton, newton_lepton,       &
            mroot_newton, newton_barpi,       &
            newton_charg, fgsl_multiroot,     &
            fsvc_pi_3d_binc, fetas_binc,      &
            fgsl_root, fsvc_pi_1d_binc,       &
            fbose_pi_1d_binc, fbose_pi_bequi_binc
contains
 
  subroutine betaequi (nb, temp, np_in, muons, b2, chat, np_out, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding, dp=>c_double
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use bareos_mod, only: chembar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint
    implicit none
    ! I/O variables
    real(dp), intent(in) :: nb, temp, np_in, b2(2)
    logical,  intent(in) :: muons
    real(dp), intent(out) :: chat, np_out
    integer,  intent(out) :: istat
    ! internal variables
    real(dp) :: munuc(2), mue, mumu, mupi, muhat
    real(dp) :: np, ne(0:2), nmu(0:2), vpot(2)
    real(dp) :: npi, npit, nnpi, nppi
    real(dp), pointer :: xl_roots(:), xh_roots(:)
    integer :: nroots
    integer :: iflag_baryon
    logical  :: chemeq_fail = .false.
    ! FGSL variables
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 100
    real(fgsl_double) :: ra, xlo, xhi
    type(c_ptr) :: ptr
    type(fgsl_function) :: stdfunc
    type(fgsl_root_fsolver) :: root_fslv
    integer(fgsl_int) :: status, i
    character(kind=fgsl_char,len=fgsl_strmax) :: name
  
    istat = 0
  
    call find_all_roots (0.d0, nb, froot, [0.0_dp], 500, xl_roots, xh_roots, nroots)
    if (nroots == 0) then
       write(error_unit,'("WARNING: No root found.")')
    else if (nroots > 1) then
       xlo = maxval(xl_roots)
       xhi = maxval(xh_roots)
#if (DEBUG > 0)
       write(error_unit,'("WARNING: More than one root found.")')
       write(error_unit,*) "WARNING: xl_root(:) = ", xl_roots(1), xl_roots(2)
       write(error_unit,*) "WARNING: xh_root(:) = ", xh_roots(1), xh_roots(2)
#endif
    else
       xlo = xl_roots(1)
       xhi = xh_roots(1)
       !print'(" :: xl_root, xh_root: ",2(1x,g0))', xlo, xhi
    end if
#if (DEBUG > 0)
    write(error_unit,'("nobetaequi_brent: Root guessed interval: ",2(1x,g0))') xlo, xhi
#endif
    
    stdfunc = fgsl_function_init(chemeq, ptr)
    root_fslv = fgsl_root_fsolver_alloc(fgsl_root_fsolver_brent)
    status = fgsl_root_fsolver_set(root_fslv, stdfunc, xlo, xhi)
    if (status /= fgsl_success) then
      istat = 1
      write(output_unit,'("betaequi: fgsl: Brent beta-equi: extremes do not straddle! ", i1)') istat
      return
    end if
    name = fgsl_root_fsolver_name(root_fslv)
    
    chemeq_fail = .false.
    i = 0
    do
       i = i + 1
       ! make one interation
       status = fgsl_root_fsolver_iterate(root_fslv)
  
       ! check status of iteration
       if (status /= fgsl_success .or. i > itmax_root) then
          if (status == -2) then
             write(output_unit,'("WARNING: betaequi: Iteration has not converged: ",i2)') status
          else if (status == 27) then
             write(output_unit,'("WARNING: betaequi: Iteration: no progress towards solution: ",i2)') status
          else
             write(output_unit,'("WARNING: betaequi: Iteration filed with code: ",i2)') status
          end if
          istat = status
          exit
       end if
  
       ! get the value of the root (ra) and the interval (xlo, xhi)
       ra = fgsl_root_fsolver_root(root_fslv)
       xlo = fgsl_root_fsolver_x_lower(root_fslv)
       xhi = fgsl_root_fsolver_x_upper(root_fslv)
       status = fgsl_root_test_interval (xlo, xhi, 0.0_fgsl_double, eps)
       !write(*,*) ' >> ', i, ra, xlo, xhi, status
       
       if (status == fgsl_success) then
          istat = status
          exit
       end if
    end do
    call fgsl_root_fsolver_free(root_fslv)
    call fgsl_function_free(stdfunc)
  
    chat = munuc(1) - munuc(2)
    np_out = ra
    istat = 0
  
  contains
  
    function chemeq (x, params) bind(c)
      real(dp), value :: x
      type(c_ptr), value :: params
      real(dp) :: chemeq
  
      chemeq_fail = .false.
      
      np = x
      call chembar (nb, temp, np, munuc(1), munuc(2), iflag_baryon)
      
      muhat = munuc(1) - munuc(2)
      ! for the electrons
      mue = muhat 
      call abundlep (memev, temp, mue, ne(0), ne(1), ne(2))
      ! for the muons
      if (muons) then
         mumu = muhat
         call abundlep (mmumev, temp, mumu, nmu(0), nmu(1), nmu(2))
      else
         mumu = 0.0_dp
         nmu(:) = 0.0_dp
      end if
   
      np = ne(0) + nmu(0)
      chemeq = np - x 
    end function chemeq
  
    function froot (x, params)
      real(dp), value :: x
      type(c_ptr), value :: params
      real(dp) :: froot
  
      chemeq_fail = .false.
      
      np = x
      call chembar (nb, temp, np, munuc(1), munuc(2), iflag_baryon)
      
      muhat = munuc(1) - munuc(2)
      ! for the electrons
      mue = muhat 
      call abundlep (memev, temp, mue, ne(0), ne(1), ne(2))
      ! for the muons
      if (muons) then
         mumu = muhat
         call abundlep (mmumev, temp, mumu, nmu(0), nmu(1), nmu(2))
      else
         mumu = 0.0_dp
         nmu(:) = 0.0_dp
      end if
   
      np = ne(0) + nmu(0)
      froot = np - x 
    end function froot
    
  end subroutine betaequi
  
  subroutine betaequipi (nb, temp, np_in, muons, b2, eta, np_out, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding, dp=>c_double
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint_eta
    use skyrme_mod, only: skyrme_model
    implicit none
    ! I/O variables
    real(dp), intent(in) :: nb, temp, np_in, b2(2)
    logical,  intent(in) :: muons
    real(dp), intent(out) :: np_out
    integer,  intent(out) :: istat
    type(nucleons), intent(out) :: eta
    ! internal variables
    real(dp) :: mue, mumu, mupi, muhat
    real(dp) :: ne(0:2), nmu(0:2)
    real(dp) :: npi, dummy, smeff(2), supot(2)
    real(dp), parameter :: delta = mnmev - mpmev
    integer :: iflag_baryon
    logical  :: chemeq_fail = .false.
    type(nucleons) :: meff, upot
    ! FGSL variables
    real(fgsl_double), parameter :: ftol = 1.0d-5
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 500
    real(fgsl_double), target :: fpar(3), xv(3)
    integer(fgsl_size_t) :: nrt
    type(c_ptr) :: ptr
    type(fgsl_multiroot_function) :: mroot_f
    type(fgsl_multiroot_fsolver) :: mroot_fslv
    type(fgsl_vector) :: xvec, fvec
    integer(fgsl_int) :: status, i
    real(fgsl_double), pointer :: fptr(:), xptr(:)
  
    istat = 0
   
    ! initial values
    call etabar (nb, temp, np_in, eta%n, eta%p, iflag_baryon)
  
    ! fsolver
    nrt = 3
    mroot_fslv = fgsl_multiroot_fsolver_alloc(fgsl_multiroot_fsolver_hybrids, nrt)
    
    ! initialize function object
    ptr = c_loc(fpar)
    mroot_f = fgsl_multiroot_function_init(chemeqpi, nrt, ptr)
    
    ! inital guesses
    xv(1:3) = (/ eta%n, eta%p, np_in /)
    xvec = fgsl_vector_init(1.0_fgsl_double)
    
    status = fgsl_vector_align(xv,nrt,xvec,nrt,0_fgsl_size_t,1_fgsl_size_t)
    status = fgsl_multiroot_fsolver_set(mroot_fslv, mroot_f, xvec)
    
    fvec = fgsl_multiroot_fsolver_f(mroot_fslv)
    status = fgsl_vector_align(fptr, fvec)
    call fgsl_vector_free(xvec)
    
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    status = fgsl_vector_align(xptr, xvec)
    
    if (status /= fgsl_success) then
      istat = 1
      write(output_unit,'("betaequpi: fgsl: Failed! ", i1)') istat
      return
    end if
    
    chemeq_fail = .false.
    
    i = 0
    do
       i = i + 1
       status = fgsl_multiroot_fsolver_iterate(mroot_fslv);
       if (i > itmax_root) then
          status = 11
          istat = status
          call test_residual_less_restrictive (ftol)
          if (istat == 0) exit
          write(output_unit,'("INFO: fgsl_multiroot: Iteration exceeded max iter: ",i2)') istat
          exit
       else if (status == fgsl_efactor) then
          istat = fgsl_efactor
          exit
       else if (status /= fgsl_success) then
          istat = status
          call test_residual_less_restrictive (ftol)
          if (istat == 0) exit
          write(output_unit,'("INFO: fgsl_multiroot: Iteration reports stat: ",i2)') istat
          !write(6, '(1x,i2,1x,3(1x,g0),1x,3(1x,g0))') i, xptr(1:3), fptr(1:3)
          exit
       end if
       
       status = fgsl_multiroot_test_residual(fvec, eps)
       if (status == fgsl_success) then 
          !write(6, '(1x,i2,1x,3(1f12.5),1x,3(1f12.5))') i, xptr(1:3), fptr(1:3)
          exit
       end if
    end do
  
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    
    eta%n  = xptr(1)
    eta%p  = xptr(2)
    np_out = xptr(3)
      
    !chat = temp * (eta%n - eta%p) + delta + upot%n - upot%p 
  
    call fgsl_multiroot_fsolver_free(mroot_fslv)
    call fgsl_multiroot_function_free(mroot_f)
  
  contains
  
  ! function to find the root(s) (beta equilibrium)
  ! μn - μp - μhat = 0
    function chemeqpi (x, params, f) bind(c)
      use defs_mod
      ! the independent variables are as follows:
      ! xv(1) = eta neutrons
      ! xv(2) = eta protons
      ! xv(3) = np
      type(c_ptr), value :: x, params, f
      integer(c_int) :: chemeqpi
      type(fgsl_vector) :: fx, ff
      real(fgsl_double), pointer :: xv(:), yv(:)!, par(:)
      integer(fgsl_int) :: status
      ! internal variables
      real(dp) :: dnn, dnp, np
      type(nucleons) :: n, meff, upot
     
      ! FGSL stuff ...
      call fgsl_obj_c_ptr(fx, x)
      call fgsl_obj_c_ptr(ff, f)
      
      status = fgsl_vector_align(xv, fx)
      status = fgsl_vector_align(yv, ff)
  
      chemeq_fail = .false.
      
  
      ! redefine independent variable for readability
      eta%n = xv(1)
      eta%p = xv(2)
      np    = xv(3)
      
      ! call Skyrme to get baryons potentials
      block
        real(dp) :: spres, sedens
        call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
      end block
  
      ! nucleon abundances without interaction with pions
      n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
      n%p = abundbar (nb, temp,      np, eta%p, mpmev)
  
      ! nucleon abundances due to interaction with pions
      dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2(1))
      dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2(2))
  
      muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
      
      ! for the electrons
      mue = muhat
      call abundlep (memev, temp, mue, ne(0), ne(1), ne(2))
      
      ! for the muons
      if (muons) then
         mumu = muhat
         call abundlep (mmumev, temp, mumu, nmu(0), nmu(1), nmu(2))
      else
         mumu = 0.0_dp
         nmu(:) = 0.0_dp
      end if
   
      ! for pions
      mupi = muhat
      npi = abundpi (temp, mupi)
      
      yv(1) = nb - np - n%n - dnn
      yv(2) = np - n%p - dnp
      yv(3) = np - ne(0) - nmu(0) - npi - dnn - dnp
      
      chemeqpi = fgsl_success 
    end function chemeqpi
    
    subroutine test_residual_less_restrictive(tolval)
      real(dp), intent(in) :: tolval
      if (sum([ (abs (fptr(i)), i=1, size(fptr)) ]) < tolval) then
         istat = 0
         write(6,'("INFO: fgsl_multiroot: tolerance reduced: ",g0)') tolval
      end if
    end subroutine test_residual_less_restrictive

  end subroutine betaequipi
  
  subroutine betaequipi_bose (nb, temp, np_in, muons, b2, eta, np_out, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding, dp=>c_double
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint_eta
    use skyrme_mod, only: skyrme_model
    implicit none
    ! I/O variables
    real(dp), intent(in) :: nb, temp, np_in, b2(2)
    logical,  intent(in) :: muons
    real(dp), intent(out) :: np_out
    integer,  intent(out) :: istat
    type(nucleons), intent(out) :: eta
    ! internal variables
    real(dp) :: mue, mumu, mupi, muhat
    real(dp) :: ne(0:2), nmu(0:2)
    real(dp) :: npi, dummy, smeff(2), supot(2)
    real(dp), parameter :: delta = mnmev - mpmev
    integer :: iflag_baryon
    logical  :: chemeq_fail = .false.
    type(nucleons) :: meff, upot
    ! FGSL variables
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 100
    real(fgsl_double), target :: fpar(3), xv(3)
    integer(fgsl_size_t) :: nrt
    type(c_ptr) :: ptr
    type(fgsl_multiroot_function) :: mroot_f
    type(fgsl_multiroot_fsolver) :: mroot_fslv
    type(fgsl_vector) :: xvec, fvec
    integer(fgsl_int) :: status, i
    real(fgsl_double), pointer :: fptr(:), xptr(:)
  
    istat = 0
   
    ! initial values
    call etabar (nb, temp, np_in, eta%n, eta%p, iflag_baryon)
  
    ! fsolver
    nrt = 3
    mroot_fslv = fgsl_multiroot_fsolver_alloc(fgsl_multiroot_fsolver_hybrids, nrt)
    
    ! initialize function object
    ptr = c_loc(fpar)
    mroot_f = fgsl_multiroot_function_init(chemeqpi, nrt, ptr)
    
    ! inital guesses
    xv(1:3) = (/ eta%n, eta%p, np_in /)
    xvec = fgsl_vector_init(1.0_fgsl_double)
    
    status = fgsl_vector_align(xv,nrt,xvec,nrt,0_fgsl_size_t,1_fgsl_size_t)
    status = fgsl_multiroot_fsolver_set(mroot_fslv, mroot_f, xvec)
    
    fvec = fgsl_multiroot_fsolver_f(mroot_fslv)
    status = fgsl_vector_align(fptr, fvec)
    call fgsl_vector_free(xvec)
    
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    status = fgsl_vector_align(xptr, xvec)
    
    if (status /= fgsl_success) then
      istat = 1
      write(output_unit,'("betaequpi: fgsl: Failed! ", i1)') istat
      return
    end if
    
    chemeq_fail = .false.
    
    i = 0
    do
       i = i + 1
       status = fgsl_multiroot_fsolver_iterate(mroot_fslv);
       if (status /= fgsl_success .or. i > itmax_root) then
          write(output_unit,'("WARNING: fgsl: iteration reports stat: ",i2)') status
          exit
       end if
       status = fgsl_multiroot_test_residual(fvec, eps)
       if (status == fgsl_success) then 
          !write(6, '(1x,i2,1x,3(1f12.5),1x,3(1f12.5))') i, xptr(1:3), fptr(1:3)
          exit
       end if
    end do
  
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    
    eta%n  = xptr(1)
    eta%p  = xptr(2)
    np_out = xptr(3)
      
    !chat = temp * (eta%n - eta%p) + delta + upot%n - upot%p 
  
    call fgsl_multiroot_fsolver_free(mroot_fslv)
    call fgsl_multiroot_function_free(mroot_f)
  
  contains
  
  ! function to find the root(s) (beta equilibrium)
  ! μn - μp - μhat = 0
    function chemeqpi (x, params, f) bind(c)
      use defs_mod
      ! the independent variables are as follows:
      ! xv(1) = eta neutrons
      ! xv(2) = eta protons
      ! xv(3) = np
      type(c_ptr), value :: x, params, f
      integer(c_int) :: chemeqpi
      type(fgsl_vector) :: fx, ff
      real(fgsl_double), pointer :: xv(:), yv(:)!, par(:)
      integer(fgsl_int) :: status
      ! internal variables
      real(dp) :: dnn, dnp, np
      type(nucleons) :: n, meff, upot
     
      ! FGSL stuff ...
      call fgsl_obj_c_ptr(fx, x)
      call fgsl_obj_c_ptr(ff, f)
      
      status = fgsl_vector_align(xv, fx)
      status = fgsl_vector_align(yv, ff)
  
      chemeq_fail = .false.
      
  
      ! redefine independent variable for readability
      eta%n = xv(1)
      eta%p = xv(2)
      np    = xv(3)
      
      ! call Skyrme to get baryons potentials
      block
        real(dp) :: spres, sedens
        call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
      end block
  
      ! nucleon abundances without interaction with pions
      n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
      n%p = abundbar (nb, temp,      np, eta%p, mpmev)
  
      ! nucleon abundances due to interaction with pions
      dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2(1))
      dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2(2))
  
      muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
      
      ! for the electrons
      mue = muhat
      call abundlep (memev, temp, mue, ne(0), ne(1), ne(2))
      
      ! for the muons
      if (muons) then
         mumu = muhat
         call abundlep (mmumev, temp, mumu, nmu(0), nmu(1), nmu(2))
      else
         mumu = 0.0_dp
         nmu(:) = 0.0_dp
      end if
   
      ! for pions
      mupi = muhat
      npi = abundpi (temp, mupi)
      
      yv(1) = nb - np - n%n - dnn
      yv(2) = np - n%p - dnp
      yv(3) = np - ne(0) - nmu(0) - npi - dnn - dnp
      
      chemeqpi = fgsl_success 
    end function chemeqpi

  end subroutine betaequipi_bose
  

  subroutine nobetaequi (part, nb, temp, ntot, guess, sroot, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, error_unit
    use, intrinsic :: iso_c_binding, dp=>c_double
    use fgsl
    use constants
    use bareos_mod, only: abundbar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi
    implicit none
    ! I/O variables
    integer,  intent(in)  :: part
    real(dp), intent(in)  :: nb, temp, ntot, guess
    real(dp), intent(out) :: sroot
    integer,  intent(out) :: istat
    ! internal variables
    integer :: iflag
    ! FGSL variables
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 200
    real(fgsl_double), allocatable, target :: fpar(:), xv(:)
    integer(fgsl_size_t) :: nrt
    type(c_ptr) :: ptr
    type(fgsl_multiroot_function) :: mroot_f
    type(fgsl_multiroot_fsolver) :: mroot_fslv
    type(fgsl_vector) :: xvec, fvec
    integer(fgsl_int) :: status, i
    real(fgsl_double), pointer :: fptr(:), xptr(:)
  
    istat = 0
   
    ! fsolver
    nrt = 1
    mroot_fslv = fgsl_multiroot_fsolver_alloc(fgsl_multiroot_fsolver_hybrids, nrt)
    
    allocate(fpar(nrt), xv(nrt))
    ! initialize function object
    ptr = c_loc(fpar)
    mroot_f = fgsl_multiroot_function_init(froot, nrt, ptr)
  
    ! inital guesses
    xv(1) = guess
    xvec = fgsl_vector_init(1.0_fgsl_double)
    
    status = fgsl_vector_align(xv,nrt,xvec,nrt,0_fgsl_size_t,1_fgsl_size_t)
    status = fgsl_multiroot_fsolver_set(mroot_fslv, mroot_f, xvec)
    
    fvec = fgsl_multiroot_fsolver_f(mroot_fslv)
    status = fgsl_vector_align(fptr, fvec)
    call fgsl_vector_free(xvec)
    
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    status = fgsl_vector_align(xptr, xvec)
    
    if (status /= fgsl_success) then
      istat = status
      write(output_unit,'("betaequpi: fgsl: Failed! ", i1)') istat
      return
    end if
    
    i = 0
    do
       i = i + 1
       status = fgsl_multiroot_fsolver_iterate(mroot_fslv);
       if (status /= fgsl_success .or. i > itmax_root) then
          write(error_unit,'("WARNING: fgsl: iteration reports stat: ",3(1x,i3))') status, i, part
          istat = status
          exit
       end if
       status = fgsl_multiroot_test_residual(fvec, eps)
       !write(6, *) i, xptr(1), fptr(1)
       if (status == fgsl_success) then 
          !write(6, '(1x,i2,1x,3(1f12.5),1x,3(1f12.5))') i, xptr(1), fptr(1)
          !write(6, *) i, xptr(1), fptr(1)
          istat = status
          exit
       end if
    end do
  
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    
    sroot  = xptr(1)
      
    call fgsl_multiroot_fsolver_free(mroot_fslv)
    call fgsl_multiroot_function_free(mroot_f)
    
    if (allocated(fpar)) deallocate(fpar)
    if (allocated(xv))   deallocate(xv)
  
  contains
  
    function froot (x, params, f) bind(c)
      type(c_ptr), value :: x, params, f
      integer(c_int) :: froot
      type(fgsl_vector) :: fx, ff
      real(fgsl_double), pointer :: xv(:), yv(:)
      integer(fgsl_int) :: status
      real(dp) :: nval, npar, nant, dnn, dnp
     
      ! FGSL stuff ...
      call fgsl_obj_c_ptr(fx, x)
      call fgsl_obj_c_ptr(ff, f)
      
      status = fgsl_vector_align(xv, fx)
      status = fgsl_vector_align(yv, ff)
      if (part == 3) write(73,*) nb, xv(1), nval, yv(1)
  
      if (part == 1) then
         nval = abundbar (nb, temp, ntot, xv(1), mnmev)
      else if (part == 2) then
         nval = abundbar (nb, temp, ntot, xv(1), mpmev)
      else if (part == 3) then
         call abundlep (memev, temp, xv(1), nval, npar, nant)
      else if (part == 4) then
         call abundlep (mmumev, temp, xv(1), nval, npar, nant)
      else if (part == 5) then
         nval = abundpi (temp, xv(1))
      end if
      
      yv(1) = (ntot - nval) / nb 
   
      froot = fgsl_success 
    end function froot
  
  end subroutine nobetaequi
  
  subroutine nobetaequipi (nb, temp, np_in, muons, ne_in, nmu_in, b2, eta, np_out, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, dp=>real64
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint_eta
    use skyrme_mod, only: skyrme_model
    implicit none
    ! I/O variables
    real(dp), intent(in) :: nb, temp, np_in, b2(2)
    logical,  intent(in) :: muons
    real(dp), intent(in), optional :: ne_in, nmu_in
    real(dp), intent(out) :: np_out
    integer,  intent(out) :: istat
    type(nucleons), intent(out) :: eta
    ! internal variables
    real(dp) :: mue, mumu, mupi, muhat
    real(dp) :: ne(0:2), nmu(0:2)
    real(dp) :: npi, dummy, smeff(2), supot(2)
    real(dp), parameter :: delta = mnmev - mpmev
    integer :: iflag_baryon
    type(nucleons) :: meff, upot
    ! FGSL variables
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 100
    real(fgsl_double), target :: fpar(3), xv(3)
    integer(fgsl_size_t) :: nrt
    type(c_ptr) :: ptr
    type(fgsl_multiroot_function) :: mroot_f
    type(fgsl_multiroot_fsolver) :: mroot_fslv
    type(fgsl_vector) :: xvec, fvec
    integer(fgsl_int) :: status, i
    real(fgsl_double), pointer :: fptr(:), xptr(:)
  
    istat = 0
   
    ! initial values
    call etabar (nb, temp, np_in, eta%n, eta%p, iflag_baryon)
#if (DEBUG > 0)
    write(output_unit,'("DEBUG: nb,T,np:",3(1x,g0))') nb, temp, np_in
    write(output_unit,'("DEBUG: eta:",2(1x,g0))') eta%n, eta%p
#endif
  
    ! fsolver
    nrt = 3
    mroot_fslv = fgsl_multiroot_fsolver_alloc(fgsl_multiroot_fsolver_hybrids, nrt)
    
    ! initialize function object
    ptr = c_loc(fpar)
    mroot_f = fgsl_multiroot_function_init(frootpi, nrt, ptr)
    
    ! inital guesses
    xv(1:3) = (/ eta%n, eta%p, np_in /)
    xvec = fgsl_vector_init(1.0_fgsl_double)
    
    status = fgsl_vector_align(xv,nrt,xvec,nrt,0_fgsl_size_t,1_fgsl_size_t)
    status = fgsl_multiroot_fsolver_set(mroot_fslv, mroot_f, xvec)
    
    fvec = fgsl_multiroot_fsolver_f(mroot_fslv)
    status = fgsl_vector_align(fptr, fvec)
    call fgsl_vector_free(xvec)
    
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    status = fgsl_vector_align(xptr, xvec)
    
    if (status /= fgsl_success) then
      istat = status
      write(output_unit,'("betaequipi: fgsl: Failed! ", i1)') istat
      return
    end if
    
    i = 0
    do
       i = i + 1
       status = fgsl_multiroot_fsolver_iterate(mroot_fslv);
       if (status /= fgsl_success .or. i > itmax_root) then
          write(output_unit,'("WARNING: nobetaequipi: fgsl: iteration reports stat: ",i2)') status
          !write(6, '(1x,i2,1x,3(1x,g0),1x,3(1x,g0))') i, xptr(1:3), fptr(1:3)
          istat = status
          exit
       end if
       status = fgsl_multiroot_test_residual(fvec, eps)
       write(6,*) i, xptr(1:3), fptr(1:3)
       if (status == fgsl_success) then 
          istat = status
          exit
       end if
    end do
  
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    
    eta%n  = xptr(1)
    eta%p  = xptr(2)
    np_out = xptr(3)
      
    !chat = temp * (eta%n - eta%p) + delta + upot%n - upot%p 
  
    call fgsl_multiroot_fsolver_free(mroot_fslv)
    call fgsl_multiroot_function_free(mroot_f)
  
  
  contains
  
  ! function to find the root(s) (beta equilibrium)
  ! μn - μp - μhat = 0
    function frootpi (x, params, f) bind(c)
      use defs_mod
      ! the independent variables are as follows:
      ! xv(1) = eta neutrons
      ! xv(2) = eta protons
      ! xv(3) = np
      type(c_ptr), value :: x, params, f
      integer(c_int) :: frootpi
      type(fgsl_vector) :: fx, ff
      real(fgsl_double), pointer :: xv(:), yv(:)!, par(:)
      integer(fgsl_int) :: status
      ! internal variables
      real(dp) :: dnn, dnp, np
      type(nucleons) :: n
     
      ! FGSL stuff ...
      call fgsl_obj_c_ptr(fx, x)
      call fgsl_obj_c_ptr(ff, f)
      
      status = fgsl_vector_align(xv, fx)
      status = fgsl_vector_align(yv, ff)
      
 
      ! check for NANs
      !if (ieee_is_nan(xv(1))) stop 'ERROR: nobetaequipi: etan is NAN'
      !if (ieee_is_nan(xv(2))) stop 'ERROR: nobetaequipi: etan is NAN'
      !if (ieee_is_nan(xv(3))) stop 'ERROR: nobetaequipi: np is NAN'

      ! redefine independent variable for readability
      eta%n = xv(1)
      eta%p = xv(2)
      np    = xv(3)
      !print'("DEBUG: etan etap, np: ",3(1x,g0))', eta%n, eta%p, np
      
      ! call Skyrme to get baryons potentials
      block
        real(dp) :: spres, sedens
        call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
      end block
  
      ! nucleon abundances without interaction with pions
      n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
      n%p = abundbar (nb, temp,      np, eta%p, mpmev)
  
      ! nucleon abundances due to interaction with pions
      dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2(1))
      dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2(2))
  
      muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
      
      ! for pions
      mupi = muhat
      npi = abundpi (temp, mupi)
      
      yv(1) = (np - ne_in - nmu_in - npi - dnn - dnp) !/ nb
      yv(2) = (np - n%p - dnp)      !/ nb
      yv(3) = (nb - np - n%n - dnn) !/ nb

      frootpi = fgsl_success 
    end function frootpi
  
  end subroutine nobetaequipi
  
  subroutine nobetaequi_brent (part, nb, temp, ntot, guess, sroot, istat)
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, error_unit
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding, dp=>c_double
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use bareos_mod, only: chembar, abundbar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint
    implicit none
    ! I/O variables
    integer,  intent(in) :: part
    real(dp), intent(in) :: nb, temp, ntot, guess(2)
    real(dp), intent(out) :: sroot
    integer,  intent(out) :: istat
    ! internal variables
    real(dp) :: munuc(2), mue, mumu, mupi, muhat
    real(dp) :: np, ne(0:2), nmu(0:2)
    real(dp) :: npi, npit, nnpi, nppi
    real(dp), pointer :: xl_roots(:), xh_roots(:)
    integer :: nroots
    integer :: iflag
    ! FGSL variables
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 100
    real(fgsl_double) :: ra, xlo, xhi
    type(c_ptr) :: ptr
    type(fgsl_function) :: stdfunc
    type(fgsl_root_fsolver) :: root_fslv
    integer(fgsl_int) :: status, i
    character(kind=fgsl_char,len=fgsl_strmax) :: name
  
    istat = 0
  
    call find_all_roots (guess(1), guess(2), froot, [0.0_dp], 500, xl_roots, xh_roots, nroots)
    if (nroots == 0) then
       write(error_unit,'("WARNING: No root found.")')
    else if (nroots > 1) then
       xlo = maxval(xl_roots)
       xhi = maxval(xh_roots)
#if (DEBUG > 0)
       write(error_unit,'("WARNING: More than one root found.")')
       write(error_unit,*) "WARNING: xl_root(:) = ", xl_roots(1), xl_roots(2)
       write(error_unit,*) "WARNING: xh_root(:) = ", xh_roots(1), xh_roots(2)
#endif
    else
       xlo = xl_roots(1)
       xhi = xh_roots(1)
       !print'(" :: xl_root, xh_root: ",2(1x,g0))', xlo, xhi
    end if
#if (DEBUG > 0)
    write(error_unit,'("nobetaequi_brent: Root guessed interval: ",2(1x,g0))') xlo, xhi
#endif
    
    stdfunc = fgsl_function_init(froot_binc, ptr)
    root_fslv = fgsl_root_fsolver_alloc(fgsl_root_fsolver_brent)
    status = fgsl_root_fsolver_set(root_fslv, stdfunc, xlo, xhi)
    if (status /= fgsl_success) then
      istat = 1
      write(error_unit,'("nobetaequi_brent: fgsl: Brent beta-equi: extremes do not straddle! ", i1)') istat
      return
    end if
    name = fgsl_root_fsolver_name(root_fslv)
    
    i = 0
    do
       i = i + 1
       ! make one interation
       status = fgsl_root_fsolver_iterate(root_fslv)
  
       ! check status of iteration
       if (status /= fgsl_success .or. i > itmax_root) then
          if (status == -2) then
             write(error_unit,'("WARNING: nobetaequi_brent: Iteration has not converged: ",2(1x,i3))') status, i
          else if (status == 27) then
             write(error_unit,'("WARNING: nobetaequi_brent: Iteration: no progress towards solution: ",2(1x,i3))') status, i
          else
             write(error_unit,'("WARNING: nobetaequi_brent: Iteration filed with code: ",2(1x,i3))') status, i
          end if
          istat = status
          exit
       end if
  
       ! get the value of the root (ra) and the interval (xlo, xhi)
       ra = fgsl_root_fsolver_root(root_fslv)
       xlo = fgsl_root_fsolver_x_lower(root_fslv)
       xhi = fgsl_root_fsolver_x_upper(root_fslv)
       status = fgsl_root_test_interval (xlo, xhi, 0.0_fgsl_double, eps)
       write(*,*) ' >> ', i, ra
       
       if (status == fgsl_success) then
          istat = status
          exit
       end if
    end do
    call fgsl_root_fsolver_free(root_fslv)
    call fgsl_function_free(stdfunc)
  
    sroot = ra
    istat = 0
  
  contains
  
    function froot_binc (x, params) bind(c)
      real(dp), value :: x
      type(c_ptr), value :: params
      real(dp) :: froot_binc, nval, npar, nant
  
      if (part == 1) then
         nval = abundbar (nb, temp, ntot, x, mnmev)
      else if (part == 2) then
         nval = abundbar (nb, temp, ntot, x, mpmev)
      else if (part == 3) then
         call abundlep (memev, temp, x, nval, npar, nant)
      else if (part == 4) then
         call abundlep (mmumev, temp, x, nval, npar, nant)
      else if (part == 5) then
         nval = abundpi (temp, x)
      end if
   
      froot_binc = (ntot - nval) / nb
      !print'(" >> DEB N :: ",4(1x,g0))', x, chemeq, ntot, nval
      !write(321,*) x, chemeq, ntot, nval
    end function froot_binc
  
    function froot (x, params)
      real(dp), value :: x
      type(c_ptr), value :: params
      real(dp) :: froot, nval, npar, nant
  
      if (part == 1) then
         nval = abundbar (nb, temp, ntot, x, mnmev)
      else if (part == 2) then
         nval = abundbar (nb, temp, ntot, x, mpmev)
      else if (part == 3) then
         call abundlep (memev, temp, x, nval, npar, nant)
      else if (part == 4) then
         call abundlep (mmumev, temp, x, nval, npar, nant)
      else if (part == 5) then
         nval = abundpi (temp, x)
      end if
   
      froot = (ntot - nval) / nb
      !print'(" >> DEB N :: ",4(1x,g0))', x, chemeq, ntot, nval
      !write(321,*) x, chemeq, ntot, nval
    end function froot
  
  end subroutine nobetaequi_brent
    
  subroutine find_all_roots (x1, x2, func, par, n, xb1, xb2, nr)
    use,intrinsic::iso_fortran_env,dp=>real64
    use, intrinsic :: iso_c_binding
    implicit none
    ! I/O variables
    real(dp), intent(in) :: x1, x2, par(:)
    integer, intent(in)  :: n
    integer, intent(out) :: nr
    real(dp), dimension(:), pointer :: xb1, xb2
    ! FGSL variables
    real(dp), allocatable, target :: fpar(:)
    ! internal variables
    real(dp) :: dx
    real(dp), dimension(0:n) :: f, x
    logical, dimension(1:n) :: mask
    logical, save :: init = .true.
    integer :: i, dstat
    type(c_ptr) :: ptr
    interface
      function func (x, params)
        import :: dp, c_ptr
        real(dp) :: func
        real(dp), value :: x
        type(c_ptr), value :: params
      end function func
    end interface
    
    if (allocated(fpar)) deallocate(fpar)
    allocate(fpar(size(par)))
    fpar = par
    ptr  = c_loc(fpar)
  
    if (init) then
       !init = .false.
       nullify (xb1, xb2)
    end if
  
    if (associated(xb1)) deallocate(xb1)
    if (associated(xb2)) deallocate(xb2)
  
    
    dx = (x2 - x1) / n
    x = [ (x1 + dx * i, i=0, n) ]
  
    do i=0, n
       f(i) = func( x(i), ptr )
       !write(987,*) x(i), f(i)
    end do
  
    mask = f(1:n) * f(0:n-1) <= 0.d0
    nr = count(mask)
  
    allocate (xb1(nr), xb2(nr))
  
    xb1(1:nr) = pack(x(0:n-1), mask) 
    xb2(1:nr) = pack(x(1:n)  , mask) 

    if (allocated(fpar)) deallocate(fpar)
  end subroutine find_all_roots
 
!-----------------------------------------------------------------------
! **************************   NEW ROUTINES   **************************
!-----------------------------------------------------------------------
  
  subroutine fgsl_root (xgues, froot, par, roots, istat)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding, dp=>c_double
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: chembar
    use lepeos_mod, only: abundlep
    use pieos_mod,  only: abundpi, pinucint
    use pass_roots, only: eroot1, eroot2

    implicit none
    ! I/O variables
    real(dp), intent(in)  :: xgues(2), par(:)
    real(dp), allocatable, intent(out) :: roots(:)
    integer,  intent(out) :: istat
    ! FGSL variables
    real(fgsl_double), allocatable, target :: fpar(:)
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 100
    real(fgsl_double) :: ra, xlo, xhi
    type(c_ptr) :: ptr
    type(fgsl_function) :: stdfunc
    type(fgsl_root_fsolver) :: root_fslv
    integer(fgsl_int) :: status, i
    character(kind=fgsl_char,len=fgsl_strmax) :: name
    
    interface
      function froot(x, params) bind(c)
        use, intrinsic :: iso_c_binding
        real(c_double), value :: x
        type(c_ptr), value :: params
        real(c_double) :: froot 
      end function froot
    end interface
  
    istat = 0

    block
      real(dp), pointer :: xl_roots(:), xh_roots(:)
      integer :: nroots
      call find_all_roots (xgues(1), xgues(2), froot, par, 500, xl_roots, xh_roots, nroots)
      if (nroots == 0) then
         istat = fgsl_einval
         if (associated(xl_roots)) deallocate(xl_roots)
         if (associated(xh_roots)) deallocate(xh_roots)
         return
      else if (nroots > 1) then
         xlo = maxval(xl_roots)
         xhi = maxval(xh_roots)
         write(output_unit,'("WARNING: More than one root found.")')
      else
         xlo = xl_roots(1)
         xhi = xh_roots(1)
      end if
      if (associated(xl_roots)) deallocate(xl_roots)
      if (associated(xh_roots)) deallocate(xh_roots)
#if (DEBUG > 0)
      write(output_unit,'("INFO: find_all_roots: Root guessed interval: ",2(1x,g0))') xlo, xhi
#endif
    end block

    ! initial values
    !xlo = xgues(1)
    !xhi = xgues(2)

    ! initialise function object
    fpar = par
    ptr  = c_loc(fpar)
    stdfunc = fgsl_function_init(froot, ptr)
    
    ! choose solver method (Brent, ...)
    root_fslv = fgsl_root_fsolver_alloc(fgsl_root_fsolver_brent)
    status = fgsl_root_fsolver_set(root_fslv, stdfunc, xlo, xhi)
    
    if (status /= fgsl_success) then
      istat = 1
      write(output_unit,'("WARNING: fgsl_root: extremes do not straddle! ", i1)') istat
      call fgsl_root_fsolver_free(root_fslv)
      call fgsl_function_free(stdfunc)
      return
    end if
    name = fgsl_root_fsolver_name(root_fslv)
    
    i = 0
    do
       i = i + 1
       ! make one interation
       status = fgsl_root_fsolver_iterate(root_fslv)
  
       ! check status of iteration
       if (i > itmax_root) then
          status = 11
          write(output_unit,'("WARNING: fgsl_root: Iteration exceeded max iter: ",i2)') status
          exit
       else if (status /= fgsl_success) then
          if (status == -2) then
             write(output_unit,'("WARNING: fgsl_root: Iteration has not converged: ",i2)') status
          else if (status == 27) then
             if (abs(xhi-xlo)/ra < 1.e-3) then
                ra = 0.5_dp * (xhi+xlo)
                istat = 0
             end if
             write(output_unit,'("WARNING: fgsl_root: Iteration: no progress towards solution: ",i2)') status
          else
             write(output_unit,'("WARNING: fgsl_root: Iteration filed with code: ",i2)') status
          end if
          istat = status
          exit
       end if
  
       ! get the value of the root (ra) and the interval (xlo, xhi)
       ra = fgsl_root_fsolver_root(root_fslv)
       xlo = fgsl_root_fsolver_x_lower(root_fslv)
       xhi = fgsl_root_fsolver_x_upper(root_fslv)
       status = fgsl_root_test_interval (xlo, xhi, 0.0_fgsl_double, eps)
       !write(*,*) ' >> ', i, ra, xlo, xhi, status     
       if (status == fgsl_success) then
          istat = status
          exit
       end if
    end do
    call fgsl_root_fsolver_free(root_fslv)
    call fgsl_function_free(stdfunc)
  
    roots = [ra, eroot1, eroot2]
  
    if (allocated(fpar)) deallocate(fpar)
  end subroutine fgsl_root
  
  subroutine fgsl_multiroot (ndim, xgues, fmroot, par, mroot, istat)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    ! Output:
    !   - istat values:
    !       0 : good
    !       1 : bad initial guess
    !      -1 : other errors
    use, intrinsic :: iso_fortran_env, only: output_unit, input_unit, dp=>real64
    use, intrinsic :: ieee_exceptions
    use, intrinsic :: iso_c_binding
    use, intrinsic :: ieee_arithmetic, only: ieee_is_nan
    use fgsl
    use constants
    use defs_mod
    implicit none
    ! I/O variables
    integer, intent(in)  :: ndim
    real(dp), intent(in)  :: xgues(ndim), par(:)
    real(dp), allocatable, intent(out) :: mroot(:)
    integer,  intent(out) :: istat
    ! internal variables
    ! FGSL variables
    real(fgsl_double), parameter :: ftol = 1.0d-5
    real(fgsl_double), parameter :: eps = 1.0d-15
    integer(fgsl_int), parameter :: itmax_root = 500
    real(fgsl_double), allocatable, target :: fpar(:), xv(:)
    integer(fgsl_size_t) :: nrt
    type(c_ptr) :: ptr
    type(fgsl_multiroot_function) :: mroot_f
    type(fgsl_multiroot_fsolver) :: mroot_fslv
    type(fgsl_vector) :: xvec, fvec
    integer(fgsl_int) :: status, i
    real(fgsl_double), pointer :: fptr(:), xptr(:)
    
    interface
      function fmroot(x, params, f) bind(c)
        use, intrinsic :: iso_c_binding
        import :: dp
        type(c_ptr), value :: x, params, f
        integer(c_int)     :: fmroot
      end function fmroot
    end interface
  
    istat = 0
  
    nrt = ndim
    ! set multiroot solver
    mroot_fslv = fgsl_multiroot_fsolver_alloc(fgsl_multiroot_fsolver_hybrids, nrt)
    
    ! initialise function object
    fpar = par
    ptr  = c_loc(fpar)
    mroot_f = fgsl_multiroot_function_init(fmroot, nrt, ptr)
    
    ! inital guesses
    xv = xgues
    xvec = fgsl_vector_init(xv)
    
    status = fgsl_multiroot_fsolver_set(mroot_fslv, mroot_f, xvec)
    
    fvec = fgsl_multiroot_fsolver_f(mroot_fslv)
    fptr => fgsl_vector_to_fptr(fvec)
    call fgsl_vector_free(xvec)
    
    xvec = fgsl_multiroot_fsolver_root(mroot_fslv)
    xptr => fgsl_vector_to_fptr(xvec)
    
    if (status /= fgsl_success) then
      istat = status
      write(output_unit,'("WARNING: fgsl_multiroot: setup failed! ", i1)') istat
      call fgsl_multiroot_fsolver_free(mroot_fslv)
      call fgsl_multiroot_function_free(mroot_f)
      return
    end if
    
    i = 0
    do
       i = i + 1
       ! make one interation
       status = fgsl_multiroot_fsolver_iterate(mroot_fslv);
       
       ! check status of iteration
       if (i > itmax_root) then
          status = 11
          istat = status
          call test_residual_less_restrictive (ftol)
          if (istat == 0) exit
          write(output_unit,'("INFO: fgsl_multiroot: Iteration exceeded max iter: ",i2)') istat
          exit
       else if (status == fgsl_efactor) then
          istat = fgsl_efactor
          exit
       else if (status /= fgsl_success) then
          istat = status
          call test_residual_less_restrictive (ftol)
          if (istat == 0) exit
          write(output_unit,'("INFO: fgsl_multiroot: Iteration reports stat: ",i2)') istat
          !write(6, '(1x,i2,1x,3(1x,g0),1x,3(1x,g0))') i, xptr(1:3), fptr(1:3)
          exit
       end if
       
       status = fgsl_multiroot_test_residual(fvec, eps)
       !write(6,*) i, xptr(1), xptr(2), xptr(3)/fpar(1), fptr
       
       if (status == fgsl_success) then 
          istat = status
          exit
       end if
    end do
  
    
    mroot = xptr
    !write(6, '(1x,i3,1x,3(1x,g0),1x,3(1x,g0))') i, xptr(1:3), fptr(1:3)
      
    call fgsl_multiroot_fsolver_free(mroot_fslv)
    call fgsl_multiroot_function_free(mroot_f)
    
    if (allocated(fpar)) deallocate(fpar)

  contains

    subroutine test_residual_less_restrictive(tolval)
      real(dp), intent(in) :: tolval
      if (sum([ (abs (fptr(i)), i=1, size(fptr)) ]) < tolval) then
         istat = 0
         write(6,'("INFO: fgsl_multiroot: tolerance reduced: ",g0)') tolval
      end if
    end subroutine test_residual_less_restrictive

  end subroutine fgsl_multiroot
  

  function fsvc_pi_3d_binc (x, params, f) bind(c)
  ! > Multi-d function binded to C containing
  ! > required functions to solve the chemical
  ! > equilibrium between baryons and pions
  ! > interacting trhough the SCV
  !
  ! > the independent variables are as follows:
  ! > xv(1) = eta neutrons
  ! > xv(2) = eta protons
  ! > xv(3) = np
  !
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use pieos_mod,  only: abundpi, pinucint_eta
    use skyrme_mod, only: skyrme_model
    
    implicit none
    ! FGSL variables
    type(c_ptr), value :: x, params, f
    integer(c_int) :: fsvc_pi_3d_binc
    type(fgsl_vector) :: fx, ff
    real(fgsl_double), pointer :: xv(:), yv(:), par(:)
    integer(fgsl_int) :: status
    
    ! internal variables
    real(dp) :: nb, temp, ne_in, nmu_in
    real(dp) :: npi, dnn, dnp, np, muhat, mupi, dummy
    real(dp), parameter :: delta = mnmev - mpmev
    type(nucleons) :: n, eta, meff, upot, b2
  
    ! map C stuff to Fortran variables
    call fgsl_obj_c_ptr(fx, x)
    call fgsl_obj_c_ptr(ff, f)
    xv => fgsl_vector_to_fptr(fx)
    yv => fgsl_vector_to_fptr(ff)
    
    ! map the parameters
    call c_f_pointer(params, par, (/ 6 /))
    nb     = par(1)
    temp   = par(2)
    ne_in  = par(3)
    nmu_in = par(4)
    b2%n   = par(5)
    b2%p   = par(6)
   
    ! check for NANs
    !if (ieee_is_nan(xv(1))) stop 'ERROR: nobetaequipi: etan is NAN'
    !if (ieee_is_nan(xv(2))) stop 'ERROR: nobetaequipi: etan is NAN'
    !if (ieee_is_nan(xv(3))) stop 'ERROR: nobetaequipi: np is NAN'

    ! redefine independent variable for readability
    eta%n = xv(1)
    eta%p = xv(2)
    np    = xv(3)
    !print'("DEBUG: etan etap, np: ",3(1x,g0))', eta%n, eta%p, np
    
    ! call Skyrme to get baryons potentials
    block
    real(dp) :: spres, sedens
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
    
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
  
    ! nucleon abundances due to interaction with pions
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)
  
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    ! for pions
    mupi = muhat
    npi = abundpi (temp, mupi)
    
    yv(1) = (np - ne_in - nmu_in - npi - dnn - dnp) !/ nb
    yv(2) = (np - n%p - dnp)      !/ nb
    yv(3) = (nb - np - n%n - dnn) !/ nb

    fsvc_pi_3d_binc = fgsl_success 
  end function fsvc_pi_3d_binc
  
  function fsvc_pi_1d_binc (x, params) bind(c)
  ! > Multi-d function binded to C containing
  ! > required functions to solve the chemical
  ! > equilibrium between baryons and pions
  ! > interacting trhough the SCV
  !
  ! > the independent variables are as follows:
  ! > xv(1) = eta neutrons
  ! > xv(2) = eta protons
  ! > xv(3) = np
  !
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use pieos_mod,  only: abundpi, pinucint_eta
    use skyrme_mod, only: skyrme_model
    use pass_roots, only: eroot1, eroot2
    
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double) :: fsvc_pi_1d_binc
    real(fgsl_double), pointer :: par(:)
    
    ! internal variables
    real(dp), allocatable :: mroot(:)
    real(dp) :: nb, temp, ne_in, nmu_in
    real(dp) :: npi, dnn, dnp, np, muhat, mupi, dummy
    real(dp), parameter :: delta = mnmev - mpmev
    type(nucleons) :: n, eta, meff, upot, b2
    integer :: istat
  
    ! map C stuff to Fortran variables
    call c_f_pointer(params, par, (/ 6 /))
    
    ! map the parameters
    nb     = par(1)
    temp   = par(2)
    ne_in  = par(3)
    nmu_in = par(4)
    b2%n   = par(5)
    b2%p   = par(6)


    ! redefine independent variable for readability
    np = x
    
    call etabar (nb, temp, np, eta%n, eta%p, istat)

    if (allocated(mroot)) deallocate(mroot)
    call fgsl_multiroot (2, [eta%n, eta%p], fetas_binc, &
         [nb, temp, np, b2%n, b2%p], mroot, istat)
    !call mroot_newton (2, [eta%n, eta%p], newton_etabar, &
    !     [nb, temp, np, b2%n, b2%p], mroot, istat)
    eroot1 = mroot(1)   ! map roots to module-shared variables
    eroot2 = mroot(2)   ! map roots to module-shared variables 
    eta%n  = mroot(1)
    eta%p  = mroot(2)
    if (allocated(mroot)) deallocate(mroot)
    
    ! call Skyrme to get baryons potentials
    block
      real(dp) :: spres, sedens
      call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
  
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
  
    ! nucleon abundances due to interaction with pions
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)
  
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    ! for pions
    mupi = muhat
    npi = abundpi (temp, mupi)
    
    fsvc_pi_1d_binc =  (np - ne_in - nmu_in - npi - dnn - dnp) / nb
  end function fsvc_pi_1d_binc
  
  function fbose_pi_1d_binc (x, params) bind(c)
  ! > the independent variables are as follows:
  ! > xv = np
  !
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
    
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use boseos_mod, only: abundbos
    use lepeos_mod, only: abundlep
    use skyrme_mod, only: skyrme_model
    use pass_roots, only: eroot1, eroot2
    
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double)     :: fbose_pi_1d_binc
    real(fgsl_double), pointer :: par(:)
    
    ! internal variables
    real(dp) :: nb, temp, ne_in, nmu_in, npic, nec, nmuc
    real(dp) :: npi, np, muhat, mupi
    real(dp), parameter :: delta = mnmev - mpmev
    type(nucleons) :: n, eta, meff, upot, b2
    integer :: istat
  
    ! map the parameters
    call c_f_pointer(params, par, (/ 4 /))
    nb     = par(1)
    temp   = par(2)
    ne_in  = par(3)
    nmu_in = par(4)


    ! redefine independent variable for readability
    np = x
    
    call etabar (nb, temp, np, eta%n, eta%p, istat)
    
    ! call Skyrme to get baryons potentials
    block
    real(dp) :: spres, sedens
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
  
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    ! for pions
    mupi = muhat
    if (mupi >= mpimev) then
       if (mupi - mpimev > 1.e-8_dp) mupi = mpimev - 1.e-8_dp
    end if
    
    block
    real(dp) :: npim, npip
    call abundbos (mpimev, temp, mupi, npi, npim, npip)
    end block

    npic = np - ne_in - nmu_in - npi
    
    eroot1 = npic
    eroot2 = 0.0_dp
    
    fbose_pi_1d_binc = np - ne_in - nmu_in - npi
  end function fbose_pi_1d_binc
  
  function fbose_pi_bequi_binc (x, params) bind(c)
  ! > the independent variables are as follows:
  ! > xv = np
  !
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
    
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use boseos_mod, only: abundbos
    use lepeos_mod, only: abundlep
    use skyrme_mod, only: skyrme_model
    use pass_roots, only: eroot1, eroot2
    
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double)     :: fbose_pi_bequi_binc
    real(fgsl_double), pointer :: par(:)
    
    ! internal variables
    real(dp) :: nb, temp, ne_in, nmu_in, npic, nec, nmuc
    real(dp) :: npi, np, muhat, mupi, mue, mumu, ne(3), nmu(3)
    real(dp), parameter :: delta = mnmev - mpmev
    type(nucleons) :: n, eta, meff, upot, b2
    integer :: istat
  
    ! map the parameters
    call c_f_pointer(params, par, (/ 4 /))
    nb     = par(1)
    temp   = par(2)
    ne_in  = par(3)
    nmu_in = par(4)


    ! redefine independent variable for readability
    np = x
    
    call etabar (nb, temp, np, eta%n, eta%p, istat)
    
    ! call Skyrme to get baryons potentials
    block
    real(dp) :: spres, sedens
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
  
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p

    ! for the electrons
    mue = muhat
    call abundlep (memev, temp, mue, ne(0), ne(1), ne(2))
    
    ! for the muons
    if (nmu_in > 0.0_dp) then
       mumu = muhat
       call abundlep (mmumev, temp, mumu, nmu(0), nmu(1), nmu(2))
    else
       mumu = 0.0_dp
       nmu(:) = 0.0_dp
    end if
    
    
    ! for pions
    mupi = muhat
    if (mupi >= mpimev) then
       if (mupi - mpimev > 1.e-8_dp) mupi = mpimev - 1.e-8_dp
    end if
    
    block
    real(dp) :: npim, npip
    call abundbos (mpimev, temp, mupi, npi, npim, npip)
    end block

    npic = np - ne_in - nmu_in - npi
    
    eroot1 = npic
    eroot2 = 0.0_dp
    
    fbose_pi_bequi_binc = np - ne(0) - nmu(0) - npi
  end function fbose_pi_bequi_binc
   
  function fbose_pic_1d_binc (x, params) bind(c)
  ! > the independent variables are as follows:
  ! > xv = np
  !
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
    
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use bareos_mod, only: abundbar, etabar
    use boseos_mod, only: abundbos
    use lepeos_mod, only: abundlep
    use skyrme_mod, only: skyrme_model
    use pass_roots, only: eroot1, eroot2
    
    implicit none
    ! FGSL variables
    real(c_double), value :: x
    type(c_ptr), value :: params
    real(c_double) :: fbose_pic_1d_binc
    real(fgsl_double), pointer :: par(:)
    
    ! internal variables
    real(dp), allocatable :: mroot(:)
    real(dp) :: nb, temp, ne_in, nmu_in, npic, nec(3), nmuc(3)
    real(dp) :: npi, np, muhat, mupi
    real(dp), parameter :: delta = mnmev - mpmev
    type(nucleons) :: n, eta, meff, upot, b2
    integer :: istat
  
    ! map C stuff to Fortran variables
    call c_f_pointer(params, par, (/ 4 /))
    
    ! map the parameters
    nb     = par(1)
    temp   = par(2)
    ne_in  = par(3)
    nmu_in = par(4)


    ! redefine independent variable for readability
    np = x
    
    call etabar (nb, temp, np, eta%n, eta%p, istat)
    
    ! call Skyrme to get baryons potentials
    block
    real(dp) :: spres, sedens
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
  
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    ! for pions
    mupi = mpimev
    if (mupi >= mpimev) then
       mupi = mpimev
       call abundlep (memev, temp, mupi, nec(1), nec(2), nec(3))
       if (nmu_in > 0.0_dp) then
          call abundlep (mmumev, temp, mupi, nmuc(1), nmuc(2), nmuc(3))
       else
          nmuc = 0.0_dp
       end if
       npic = ne_in + nmu_in - nec(1) - nmuc(1)
    else
       npic = 0.0_dp
    end if
    
    block
    real(dp) :: npim, npip
    call abundbos (mpimev, temp, mupi, npi, npim, npip)
    end block

    eroot1 = np
    eroot2 = npic
    
    fbose_pic_1d_binc =  np - ne_in - nmu_in - npi
  end function fbose_pic_1d_binc
  
  function fetas_binc (x, params, f) bind(c)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): np
  ! > params(4): b2%n
  ! > params(5): b2%p
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: iso_c_binding
    use fgsl
    use constants
    use defs_mod
    use skyrme_mod, only: skyrme_model, skyrme_meff
    use bareos_mod, only: abundbar
    use pieos_mod,  only: abundpi, pinucint_eta 
    use fermi_integrals_mod, only: fermi_minus_one_half, fermi_one_half
    implicit none
    ! FGSL variables
    type(c_ptr), value :: x, params, f
    integer(c_int) :: fetas_binc
    type(fgsl_vector) :: fx, ff
    real(fgsl_double), pointer :: xv(:), yv(:), par(:)
    integer(fgsl_int) :: status
    
    real(dp) :: nb, temp, np
    real(dp) :: npi, dnn, dnp, fact, dummy
    type(nucleons) :: n, eta, meff, upot, fac, b2
    real(dp), parameter :: delta = mnmev - mpmev
    integer :: i
    
    ! map C stuff to Fortran variables
    call fgsl_obj_c_ptr(fx, x)
    call fgsl_obj_c_ptr(ff, f)
    call c_f_pointer(params, par, (/ 5 /))
    xv => fgsl_vector_to_fptr(fx)
    yv => fgsl_vector_to_fptr(ff)

   
    ! rewrite independent variables with its physical variables
    eta%n = xv(1)
    eta%p = xv(2)

    ! define the parameters
    nb   = par(1)
    temp = par(2)
    np   = par(3)
    b2%n = par(4)
    b2%p = par(5)
    
    ! compute quantities for f and J
    block
    real(dp) :: spres, sedens
    call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
    
    !print'(" >> DEBUG 1: etabar: ",6(1x,g0))', eta%n, eta%p, meff%n, meff%p, upot%n, upot%p
    fact = temp/(hbarcfm*hbarcfm)
    fact = fact*fact*fact
    fact = sqrt(2.0_dp*fact)/(pi*pi)
    fac%n = fact * sqrt(meff%n)**3
    fac%p = fact * sqrt(meff%p)**3
      
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
    
    !print'(" >> DEBUG 3.1: etabar: ",6(1x,g0))', eta%n, eta%p, upot%n, upot%p, dnn, dnp
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)
    !print'(" >> DEBUG 3.2: etabar: ",6(1x,g0))', eta%n, eta%p, upot%n, upot%p, dnn, dnp

    ! set of equations
    yv(1) = (nb - np - n%n - dnn) / nb
    yv(2) = (np - n%p  - dnp)     / nb
    
    fetas_binc = fgsl_success
  end function fetas_binc

  subroutine root_newton (x_i, sub_fnewton, sparams, sroot, istat)
    ! fparams(1): mass
    ! fparams(2): temp
    ! fparams(3): n_in
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: ieee_arithmetic
    use, intrinsic :: ieee_features
    implicit none
    real(dp), intent(in)  :: x_i
    real(dp), intent(in)  :: sparams(:)
    real(dp), allocatable, intent(out) :: sroot(:)
    integer,  intent(out) :: istat
    integer,  parameter :: iter_max = 100
    real(dp), parameter :: tol = 1.0e-15_dp
    real(dp), allocatable :: opar(:)
    real(dp) :: x, dx, f, df
    integer :: i
    interface
      subroutine sub_fnewton (x, params, fo, dfo, opar)
        import :: dp
        real(dp), intent(in)  :: x, params(:)
        real(dp), intent(out) :: fo, dfo
        real(dp), allocatable, optional, intent(out) :: opar(:)
      end subroutine sub_fnewton
    end interface
    istat = 0
    if (allocated(sroot)) deallocate(sroot)
    allocate(sroot(3))
    x = x_i
    i = 0
    do
      i = i + 1
      call sub_fnewton (x, sparams, f, df, opar)
      dx = f / df
      !write(*,'(i5,4(1x,g0))') i, x, dx, f, df
      sroot(1) = x
      if (allocated(opar)) sroot(2) = opar(1)
      if (allocated(opar)) sroot(3) = opar(2)
      if (abs(dx) < tol .and. i < iter_max) then
         istat = 0
         exit
      else if (i > iter_max) then
         if (abs(dx) < 1.e-7_dp) then 
            istat = 0
            exit
         end if
         istat = 1
         write(error_unit,'("WARNING: newton: i > iter_max",i5)') i
         exit
      end if
      x = x - dx
    end do
  end subroutine root_newton

  subroutine newton_lepton (eta_in, params, f, df, opar)
    use,intrinsic::iso_fortran_env, dp=>real64
    use constants
    implicit none
    real(dp), intent(in)  :: eta_in, params(:)
    real(dp), intent(out) :: f, df
    real(dp), allocatable, optional, intent(out) :: opar(:)
    real(dp) :: fac, eta, beta, mass, temp, n_in
    real(dp) :: fe12, fe12eta, fe12beta
    real(dp) :: fe32, fe32eta, fe32beta
    real(dp) :: fp12, fp12eta, fp12beta
    real(dp) :: fp32, fp32eta, fp32beta
    
    mass = params(1)
    temp = params(2); beta = temp/mass
    n_in = params(3)

    fac = mass*sqrt(beta)/hbarcfm
    fac = sqrt(2.0_dp)*fac*fac*fac/(pi*pi)
    fac = 1.0_dp/fac
    
    eta = eta_in
    call dfermi (0.5d0, eta, beta, fe12, fe12eta, fe12beta)
    call dfermi (1.5d0, eta, beta, fe32, fe32eta, fe32beta)
    
    eta = - eta - 2.0_dp / beta
    call dfermi (0.5d0, eta, beta, fp12, fp12eta, fp12beta)
    call dfermi (1.5d0, eta, beta, fp32, fp32eta, fp32beta)
    
    f  =  fe12 - fp12 + beta * (fe32 - fp32) - fac * n_in
    df =  fe12eta + fp12eta + beta * (fe32eta + fp32eta)
    !print'(" >>> DEBG:: ",3(1x,g0))', eta_in, f, df
  end subroutine newton_lepton 

  subroutine mroot_newton (rdim, x_i, sub_mnewton, mparams, mroot, istat)
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: ieee_arithmetic
    use, intrinsic :: ieee_features
    
    implicit none
    integer,  intent(in)  :: rdim
    real(dp), intent(in)  :: x_i(:)
    real(dp), intent(in)  :: mparams(:)
    real(dp), allocatable, intent(out) :: mroot(:)
    integer,  intent(out) :: istat
    integer,  parameter :: iter_max = 100
    real(dp), parameter :: xtol = 1.0e-16_dp
    real(dp), parameter :: ftol = 1.0e-16_dp
    real(dp) :: x(rdim), dx(rdim), f(rdim), Ji(rdim,rdim), sdx, sf
    integer :: i, j
    interface
      subroutine sub_mnewton (x, params, f, Jinv)
        import :: dp
        real(dp), intent(in)  :: x(:), params(:)
        real(dp), intent(out) :: f(:), Jinv(:,:)
      end subroutine sub_mnewton
    end interface
    istat = 0
    x = x_i
    i = 0
    do
      i = i + 1
      call sub_mnewton (x, mparams, f, Ji)
      
      if (size(f) /= rdim .or. size(Ji,dim=1) /= rdim)  &
      stop 'ERROR: mroot_newton: Function and Jacobian&
           & dimension mismatch input dimension'
      
      dx  = [ (dot_product(Ji(j,:), f), j=1, rdim) ]
      sf  = sum([ (abs (f(i)), i=1, rdim) ])
      sdx = sum([ (abs(dx(i)), i=1, rdim) ])
      
      mroot = x
      !write(error_unit,'(i5,11(1x,es11.4))') i, x, f, dx, sdx, sf
      if (sdx < xtol .or. sf < ftol &
         .and. i < iter_max) then
         istat = 0
         exit
      else if (i > iter_max) then
         istat = 1
         write(error_unit,'("WARNING: multi newton: i > iter_max",i5)') i
         exit
      end if
      
      x = x - dx
    end do
  end subroutine mroot_newton

  subroutine newton_charg (x, params, f, df, opar)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: ieee_arithmetic
    use, intrinsic :: ieee_features
    use constants
    use defs_mod
    use skyrme_mod, only: skyrme_model, skyrme_meff
    use bareos_mod, only: abundbar
    use pieos_mod,  only: abundpi, pinucint_eta 
    use fermi_integrals_mod, only: fermi_minus_one_half, fermi_one_half
    implicit none
    real(dp), intent(in)  :: x, params(:)
    real(dp), intent(out) :: f, df
    real(dp), allocatable, optional, intent(out) :: opar(:)
    real(dp) :: nb, temp, np, ne_in, nmu_in
    real(dp), allocatable :: mroot(:)
    real(dp) :: npi, dnn, dnp, muhat, fact, dummy
    integer  :: istat
    type(nucleons) :: n, eta, meff, upot, fac, b2
    real(dp), parameter :: delta = mnmev - mpmev
    type(ieee_flag_type) :: ieee_flags
    logical :: ieee_flag_val
    
    ! rewrite independent variables with its physical variables
    np = x
    if (np < 0.0_dp) stop 'ERROR: newton_charg: np < 0'

    ! map the parameters
    nb     = params(1)
    temp   = params(2)
    ne_in  = params(3)
    nmu_in = params(4)
    b2%n   = params(5)
    b2%p   = params(6)
    eta%n  = params(7)
    eta%p  = params(8)

    ! find eta%n and eta%p for the given np
    if (.not.allocated(mroot)) allocate(mroot(2))
    call mroot_newton (2, [eta%n, eta%p], newton_etabar,  &
         [nb, temp, np, b2%n, b2%p],  &
         mroot, istat)
    
    if (ieee_is_nan(mroot(1)).or.ieee_is_nan(mroot(2))) &
       stop 'ERROR: newton_charg: IEEE NAN in roots'
    
    eta%n = mroot(1)
    eta%p = mroot(2)

    ! save eta%n and eta%p as output params
    if (.not.allocated(opar)) allocate(opar(2))
    opar(1) = eta%n
    opar(2) = eta%p

    ! compute quantities for f and J
    block
      real(dp) :: spres, sedens
      call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
    
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    fact = temp/(hbarcfm*hbarcfm)
    fact = fact*fact*fact
    fact = sqrt(2.0_dp*fact)/(pi*pi)
    fac%n = fact * sqrt(meff%n)**3
    fac%p = fact * sqrt(meff%p)**3
      
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
    
    npi = abundpi (temp, muhat)
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)

    ! set of equations
    f  = (np - ne_in - nmu_in - npi - dnn - dnp) !/ nb
    df = 1.0_dp
    print'("newton_charg: DEBUG: ",9(1x,g0))', fac%n, fac%p, n%n, n%p, npi, dnn, dnp, f, df
    
  end subroutine newton_charg

  subroutine newton_barpi (x, params, f, Jinv)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): ne_in
  ! > params(4): nmu_in
  ! > params(5): b2%n
  ! > params(6): b2%p
    use, intrinsic :: iso_fortran_env, dp=>real64
    use, intrinsic :: ieee_arithmetic
    use, intrinsic :: ieee_features
    use constants
    use defs_mod
    use skyrme_mod, only: skyrme_model, skyrme_meff
    use bareos_mod, only: abundbar
    use pieos_mod,  only: abundpi, pinucint_eta 
    use fermi_integrals_mod, only: fermi_minus_one_half, fermi_one_half
    implicit none
    real(dp), intent(in)  :: x(:), params(:)
    real(dp), intent(out) :: f(:), Jinv(:,:)
    real(dp) :: nb, temp, np, ne_in, nmu_in, J(3,3)
    real(dp) :: npi, dnn, dnp, muhat, fact, dummy
    integer  :: ndim
    type(nucleons) :: n, eta, meff, upot, fac, b2
    real(dp), parameter :: delta = mnmev - mpmev
    type(ieee_flag_type) :: ieee_flags
    logical :: ieee_flag_val
    
    ndim = size(x)
    
    ! rewrite independent variables with its physical variables
    eta%n = x(1)
    eta%p = x(2)
    np    = x(3)
    if (np < 0.0_dp) stop 'ERROR: multi newton: np < 0'

    ! define the parameters
    nb     = params(1)
    temp   = params(2)
    ne_in  = params(3)
    nmu_in = params(4)
    b2%n   = params(5)
    b2%p   = params(6)
    
    ! compute quantities for f and J
    block
      real(dp) :: spres, sedens
      call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
    
    muhat = temp * (eta%n - eta%p) + delta + upot%n - upot%p
    
    fact = temp/(hbarcfm*hbarcfm)
    fact = fact*fact*fact
    fact = sqrt(2.0_dp*fact)/(pi*pi)
    fac%n = fact * sqrt(meff%n)**3
    fac%p = fact * sqrt(meff%p)**3
      
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
    
    npi = abundpi (temp, muhat)
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)

    ! set of equations
    f(1) = (np - ne_in - nmu_in - npi - dnn - dnp) !/ nb
    f(2) = (np - n%p  - dnp) !/nb
    f(3) = (nb - np - n%n - dnn) !/ nb
    
    ! the Jacobian of the set of equations
    J(1,1) = - (npi + 2.0_dp*dnn + dnp) 
    J(1,2) =   npi + dnn
    J(1,3) =   1.0_dp
    J(2,1) = - dnp
    J(2,2) = - 0.5_dp * fac%p * fermi_minus_one_half(eta%p)
    J(2,3) =   1.0_dp
    J(3,1) = - 0.5_dp * fac%n * fermi_minus_one_half(eta%n) - 2.0_dp*dnn
    J(3,2) =   dnn
    J(3,3) = - 1.0_dp

    ! inverse of the Jacobian
    Jinv = matinv3 (J)
  end subroutine newton_barpi
  
  subroutine newton_etabar (x, params, f, Jinv)
  ! > params(1): nb
  ! > params(2): temp
  ! > params(3): np
  ! > params(4): b2%n
  ! > params(5): b2%p
    use, intrinsic :: iso_fortran_env, dp=>real64
    use constants
    use defs_mod
    use skyrme_mod
    use bareos_mod, only: abundbar
    use pieos_mod,  only: abundpi, pinucint_eta 
    use fermi_integrals_mod, only: fermi_minus_one_half, fermi_one_half
    implicit none
    real(dp), intent(in)  :: x(:), params(:)
    real(dp), intent(out) :: f(:), Jinv(:,:)
    real(dp) :: nb, temp, np
    real(dp) :: dnn, dnp, fact, dummy
    type(nucleons) :: n, eta, meff, upot, fac, b2
    real(dp), parameter :: delta = mnmev - mpmev
    
    ! rewrite independent variables with its physical variables
    eta%n = x(1)
    eta%p = x(2)

    ! define the parameters
    nb   = params(1)
    temp = params(2)
    np   = params(3)
    b2%n = params(4)
    b2%p = params(5)
    
    ! compute quantities for f and J
    block
      real(dp) :: spres, sedens
      call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
    end block
    
    print'(" >> DEBUG 1: eta, m, upot: ",6(1x,g0))', eta%n, eta%p, meff%n, meff%p, upot%n, upot%p
    fact = temp/(hbarcfm*hbarcfm)
    fact = fact*fact*fact
    fact = sqrt(2.0_dp*fact)/(pi*pi)
    fac%n = fact * sqrt(meff%n)**3
    fac%p = fact * sqrt(meff%p)**3
      
    ! nucleon abundances without interaction with pions
    n%n = abundbar (nb, temp, nb - np, eta%n, mnmev)
    n%p = abundbar (nb, temp,      np, eta%p, mpmev)
    
    print'(" >> DEBUG 3.1: n%n, n%p: ",2(1x,g0))', n%n, n%p
    dnn = pinucint_eta (1, temp, [eta%n, eta%p], [upot%n, upot%p], b2%n)
    dnp = pinucint_eta (2, temp, [eta%n, eta%p], [upot%n, upot%p], b2%p)
    print'(" >> DEBUG 3.2: dnn, dnp: ",2(1x,g0))', dnn, dnp

    ! set of equations
    f(1) = (nb - np - n%n - dnn)  !/ nb
    f(2) =      (np - n%p - dnp) !/ nb

    ! compute inverse Jacobian
    block
      real(dp) :: detJ, fm12n, fm12p, dfdn(2), dfdp(2)
      real(dp) :: ddnn_dn,ddnn_dp, ddnp_dn, ddnp_dp
      integer :: i

      fm12n = 0.5_dp * fac%n * fermi_minus_one_half(eta%n)
      fm12p = 0.5_dp * fac%p * fermi_minus_one_half(eta%p)

      ddnn_dn = ( 2.0_dp + dudetan%n / temp) * dnn
      ddnn_dp = (-1.0_dp + dudetap%n / temp) * dnn
      ddnp_dn = ( 1.0_dp + dudetan%n / temp) * dnp
      ddnp_dp =           (dudetap%n / temp) * dnp
      
      dfdn(1) = - fm12n - ddnn_dn
      dfdp(1) =         - ddnn_dp
      
      dfdn(2) =         - ddnp_dn
      dfdp(2) = - fm12p - ddnp_dp
      print'(">> DEBUG: df/detan: ",2(1x,g0))', dfdn
      print'(">> DEBUG: df/detap: ",2(1x,g0))', dfdp
      
      ! the inverse Jacobian of the set of equations
      detJ = dfdn(1) * dfdp(2) - dfdp(1) * dfdn(2)
      
      Jinv(1,1) =   dfdp(2); Jinv(1,2) = - dfdn(2)
      Jinv(2,1) = - dfdp(1); Jinv(2,2) =   dfdn(1)
      
      Jinv = Jinv / detJ
      
      do i=1, size(Jinv, dim=1)
         print'(" :: Jinv: ",2(1x,g0))', Jinv(i,:)
      end do
      print'(" >> DEBUG 5: eta, Jinv, detJ: ",10(1x,g0))', eta%n, eta%p, Jinv, detJ
    end block
    
  end subroutine newton_etabar

  pure function matinv3(A) result(B)
    use,intrinsic::iso_fortran_env, wp=>real64
    !! Performs a direct calculation of the inverse of a 3×3 matrix.
    implicit none
    real(wp), intent(in) :: A(3,3)   !! Matrix
    real(wp)             :: B(3,3)   !! Inverse matrix
    real(wp)             :: detinv

    ! Calculate the inverse determinant of the matrix
    detinv = 1/(A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2)&
              - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1)&
              + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1))

    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))
  end function

end module root_finder_mod
