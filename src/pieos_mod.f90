module pieos_mod
  use, intrinsic::iso_fortran_env, dp=>real64
  use fgsl
  use constants
  implicit none
  public :: abundpi, pinucint, prespi
contains
  
  function abundpi (temp, mu)
    real(dp) :: temp, mu, abundpi
    real(dp) :: a, b
    real(dp) :: besk2
    ! FGSL variables
    integer :: stat
    type(fgsl_sf_result) :: sfres
    
    ! define these parameters
    a = mu/temp; b = mpimev/temp
    
    if (b > 708._dp) then
       abundpi = 0.0_dp
       return
    end if

    ! compute modified Bessel function of second king of degree 2
    ! from FGSL library
    stat = fgsl_sf_bessel_kcn_e(2, b, sfres)
    if (stat /= fgsl_success) stop 'ERROR: fgsl: unsuccess Bessel K3 evaluation' 
    besk2 = sfres%val
    
    ! non-interacting contribution to abundance
    abundpi = hbarcfm*hbarcfm*hbarcfm*pi*pi
    abundpi = temp*mpimev*mpimev/abundpi
    abundpi = 0.5_dp * abundpi * exp(a) * besk2
  end function abundpi

  function prespi (temp, npi)
    real(dp) :: temp, npi, prespi
    prespi = temp * exp(-mpimev/temp) * npi
  end function prespi

  function enerpi (temp, mu)
    real(dp) :: temp, mu, enerpi 
    real(dp) :: a, b
    real(dp) :: besk0, besk4
    ! FGSL variables
    integer :: stat
    type(fgsl_sf_result) :: sfres
    
    ! define these parameters
    a = mu/temp; b = mpimev/temp
    
    ! compute modified Bessel functions of second king for 1 and 3 degree
    ! from FGSL library
    if (b > 708._dp) then
       besk0  = 0.0_dp
       besk4  = 0.0_dp
       enerpi = 0.0_dp
       return
    else
       stat = fgsl_sf_bessel_kc0_e(b, sfres)
       if (stat /= fgsl_success) stop 'ERROR: fgsl: unsuccess Bessel K0 evaluation' 
       besk0 = sfres%val
       stat = fgsl_sf_bessel_kcn_e(4, b, sfres)
       if (stat /= fgsl_success) stop 'ERROR: fgsl: unsuccess Bessel K4 evaluation' 
       besk4 = sfres%val
    end if

    ! non-interacting contribution to abundance
    enerpi = hbarcfm*hbarcfm*hbarcfm*pi*pi
    enerpi = (0.5_dp*mpimev)**4/enerpi
    enerpi = enerpi * exp(a-b) * (besk4-besk0)
  end function enerpi
  
  function pinucint (temp, mu, mass, b2)
    real(dp) :: temp, mu(2), mass(2), b2, pinucint
    pinucint = exp((mu(1)-mass(1)+mu(2)-mass(2))/temp)*b2
  end function pinucint

  function pinucint_eta (iso, temp, eta, upot, b2)
    real(dp), parameter :: delta = mnmev - mpmev
    real(dp) :: dn, temp, b2, pinucint_eta
    real(dp) :: eta(2), upot(2)
    integer :: iso
    if (iso == 1) then
       dn = b2 * exp(2.0_dp*eta(1) - eta(2) + &
         (2.0_dp * upot(1) - upot(2) + delta - mpimev) /temp)
    else if (iso == 2) then
       dn = b2 * exp(eta(1) + (upot(1) + delta - mpimev) / temp)
    end if
    pinucint_eta = dn
  end function pinucint_eta

end module pieos_mod
