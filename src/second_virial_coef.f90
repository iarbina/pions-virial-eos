subroutine second_virial_coef (temp_in, b2npi, b2ppi, db2npi, db2ppi)
  use,intrinsic::iso_c_binding,dp=>c_double
  use iso_fortran_env, only: output_unit
  use, intrinsic :: ieee_arithmetic
  use fgsl
  use constants
  use phase_shifts_mod
  implicit none
  
  ! I/O variables
  real(dp), intent(in) :: temp_in
  real(dp), intent(out) :: b2npi, b2ppi, db2npi, db2ppi
  
  ! internal variable
  real(dp) :: bfact, bfactn, bfactp, Mnpi, Mppi, temp
  
  ! FGSL variables
  integer(fgsl_size_t), parameter :: intlimit = 1000_fgsl_size_t
  integer(fgsl_int) :: intstat
  integer, parameter :: key = 2
  real(dp), parameter :: epsabs = 0.0d0
  real(dp), parameter :: epsrel  = 1.0d-7
  real(dp), parameter :: temptol = 1.0d-4
  !real(fgsl_double), target :: xx
  real(fgsl_double) :: intres, intabserr
  type(c_ptr) :: ptr
  type(fgsl_function) :: stdfunc
  type(fgsl_integration_workspace) :: integ_wk
  type(fgsl_error_handler_t) :: std, off

  temp = temp_in

  if (temp_in < 0.0_dp) stop 'ERROR: No negative temperatures allowed'
  if (temp_in < temptol) then
     b2npi  = 0.0_dp  
     b2ppi  = 0.0_dp  
     db2npi = 0.0_dp  
     db2ppi = 0.0_dp
     return
  else if (temp_in < 2.0_dp) then
     temp = 2.0_dp
  end if

  ! read phase shifts from phase_shifts_mod module
  call read_phase_shifts

  Mnpi = mnmev + mpimev; Mppi = mpmev + mpimev

  ! integration for n-pi second virial coefficient
  !ptr = c_loc(xx)
  integ_wk = fgsl_integration_workspace_alloc(intlimit)
  stdfunc  = fgsl_function_init(np_func, ptr)
  intstat  = fgsl_integration_qag(stdfunc, minval(Ecm), maxval(Ecm),  &
    epsabs, epsrel, intlimit, key, integ_wk, intres, intabserr)
  !print'(" >> DEBUG: Integration abs error: ",g0)', intabserr
  if (intstat /= fgsl_success)  &
  write(output_unit,'("fgsl: ERROR: QAG integration return stat: ",i1)') intstat
  call fgsl_function_free(stdfunc)
  call fgsl_integration_workspace_free(integ_wk)

  bfact = pi*hbarcmev*1.0e13_dp
  bfact = 2.0_dp*bfact*bfact*bfact
  bfactn = exp(Mnpi/temp) / bfact
  b2npi = bfactn * intres

  call ieee_error_handler (b2npi, 'b2npi')

  ! integration for p-pi second virial coefficient
  integ_wk = fgsl_integration_workspace_alloc(intlimit)
  stdfunc  = fgsl_function_init(pp_func, ptr)
  intstat  = fgsl_integration_qag(stdfunc, minval(Ecm), maxval(Ecm),  &
    epsabs, epsrel, intlimit, key, integ_wk, intres, intabserr)
  !print'(" >> DEBUG: Integration abs error: ",g0)', intabserr
  if (intstat /= fgsl_success)  &
  write(output_unit,'("fgsl: ERROR: QAG integration return stat: ",i1)') intstat
  call fgsl_function_free(stdfunc)
  call fgsl_integration_workspace_free(integ_wk)

  bfactp = exp(Mppi/temp) / bfact
  b2ppi = bfactp * intres
  
  call ieee_error_handler (b2ppi, 'b2ppi')

  ! Derivatives
  ! integration for n-pi derivative second virial coefficient
  integ_wk = fgsl_integration_workspace_alloc(intlimit)
  stdfunc  = fgsl_function_init(dnp_func, ptr)
  intstat  = fgsl_integration_qag(stdfunc, minval(Ecm), maxval(Ecm),  &
    epsabs, epsrel, intlimit, key, integ_wk, intres, intabserr)
  !print'(" >> DEBUG: Integration abs error: ",g0)', intabserr
  if (intstat /= fgsl_success)  &
  write(output_unit,'("fgsl: ERROR: QAG integration return stat: ",i1)') intstat
  call fgsl_function_free(stdfunc)
  call fgsl_integration_workspace_free(integ_wk)

  db2npi = Mnpi*b2npi - 0.5_dp*bfactn*intres 

  call ieee_error_handler (db2npi, 'db2npi')

  ! integration for p-pi derivative second virial coefficient
  integ_wk = fgsl_integration_workspace_alloc(intlimit)
  stdfunc  = fgsl_function_init(dpp_func, ptr)
  intstat  = fgsl_integration_qag(stdfunc, minval(Ecm), maxval(Ecm),  &
    epsabs, epsrel, intlimit, key, integ_wk, intres, intabserr)
  !print'(" >> DEBUG: Integration abs error: ",g0)', intabserr
  if (intstat /= fgsl_success)  &
  write(output_unit,'("fgsl: ERROR: QAG integration return stat: ",i1)') intstat
  call fgsl_function_free(stdfunc)
  call fgsl_integration_workspace_free(integ_wk)

  db2ppi = Mppi*b2ppi - 0.5_dp*bfactp*intres 
  
  call ieee_error_handler (db2ppi, 'db2ppi')

  if (temp_in < 2.0_dp) then
     b2npi  = lininterp(temp_in, [0.0_dp, temp], [0.0_dp, b2npi])
     b2ppi  = lininterp(temp_in, [0.0_dp, temp], [0.0_dp, b2ppi])
     db2npi = lininterp(temp_in, [0.0_dp, temp], [0.0_dp, db2npi])
     db2ppi = lininterp(temp_in, [0.0_dp, temp], [0.0_dp, db2ppi])
  end if

contains
  
  function lininterp(x, xl, yl)
    real(dp) :: lininterp, x, xl(2), yl(2)
    lininterp = ((xl(2)-x)*yl(1)+(x-xl(1))*yl(2))/(xl(2)-xl(1))
  end function lininterp

  subroutine ieee_error_handler (var, errmsg)
    real(dp), intent(in) :: var
    character(len=*), intent(in) :: errmsg
    if (ieee_is_nan(var) .or. .not.ieee_is_finite(var)) then
        print'("second_virial_coef: IEEE ERROR: '//errmsg//' = ", g0)', var 
        stop 'Program halted'
    end if
  end subroutine ieee_error_handler

  subroutine fgsl_sf_error (sf_stat, sf_result)
    integer(fgsl_int), intent(in) :: sf_stat
    type(fgsl_sf_result), intent(inout) :: sf_result
    if (sf_stat == 0) return
    if (sf_stat == 15) then
       sf_result%val = tiny(sf_result%val)
       write(output_unit,'("second_virial_coef: UNDERFLOW: fgsl Bessel: ", i4)') &
         sf_stat
    else if (sf_stat == 16) then
       sf_result%val = huge(sf_result%val)
       write(output_unit,'("second_virial_coef: OVERFLOW: fgsl Bessel: ", i4)')&
         sf_stat
    else
       write(output_unit,'("second_virial_coef: ERROR: fgsl Bessel: ", i4)') &
         sf_stat
       stop
    end if
  end subroutine fgsl_sf_error

  ! neutron-pion second virial integrand
  function np_func(e, params) bind(c)
    real(dp), value :: e
    type(c_ptr), value :: params
    real(dp) :: np_func
    real(dp) :: iS31, iP31, iP33
    integer, dimension(6) :: istat
    integer(fgsl_int) :: fgsl_sf_stat
    type(fgsl_sf_result) :: besk1
    ! interpolate for the needed value
    if (e < minval(Ecm) .and. e > maxval(Ecm)) stop "STOP: Interp out of range"
    call interp_phase_shifts (Ecm, S31, e, iS31, istat(2))
    call interp_phase_shifts (Ecm, P31, e, iP31, istat(5))
    call interp_phase_shifts (Ecm, P33, e, iP33, istat(6))
    
    ! WARNING: disable GSL error handler
    std = fgsl_set_error_handler_off()
    ! call fgsl bessel k1
    fgsl_sf_stat = fgsl_sf_bessel_kc1_e(e/temp, besk1)
    ! check fgsl bessel k1 returns any error
    if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk1)
    ! return back standard error handler
    off = fgsl_set_error_handler(std)
    
    np_func = e*e * besk1%val * (iS31 + 3.0_dp * (iP31+iP33))
    !write(124,*) e, iS31, iP31, iP33
  end function np_func
  
  ! proton-pion second virial integrand
  function pp_func(e, params) bind(c)
    real(dp), value :: e
    type(c_ptr), value :: params
    real(dp) :: pp_func
    real(dp) :: iS11, iS31, iP11, iP13, iP31, iP33
    integer, dimension(6) :: istat
    integer(fgsl_int) :: fgsl_sf_stat
    type(fgsl_sf_result) :: besk1
    if (e < minval(Ecm) .and. e > maxval(Ecm)) stop "STOP: Interp out of range"
    call interp_phase_shifts (Ecm, S11, e, iS11, istat(1))
    call interp_phase_shifts (Ecm, S31, e, iS31, istat(2))
    call interp_phase_shifts (Ecm, P11, e, iP11, istat(3))
    call interp_phase_shifts (Ecm, P13, e, iP13, istat(4))
    call interp_phase_shifts (Ecm, P31, e, iP31, istat(5))
    call interp_phase_shifts (Ecm, P33, e, iP33, istat(6))
    
    if (e/temp > 708.0_dp) then
       besk1%val = 0.0_dp
    else
       ! WARNING: disable GSL error handler
       std = fgsl_set_error_handler_off()
       ! call fgsl bessel k1
       fgsl_sf_stat = fgsl_sf_bessel_kc1_e(e/temp, besk1)
       ! check fgsl bessel k1 returns any error
       if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk1)
       ! return back standard error handler
       off = fgsl_set_error_handler(std)
    end if
    
    pp_func = e*e * besk1%val * (iS11+iS31 + 3.0_dp * (iP11+iP13+iP31+iP33))
    !write(125,*) e, iS11, iS31, iP11, iP13, iP31, iP33
  end function pp_func
  
  ! neutron-pion derivative second virial integrand
  function dnp_func(e, params) bind(c)
    real(dp), value :: e
    type(c_ptr), value :: params
    real(dp) :: dnp_func
    real(dp) :: iS31, iP31, iP33
    integer, dimension(6) :: istat
    integer(fgsl_int) :: fgsl_sf_stat
    type(fgsl_sf_result) :: besk0, besk2
    if (e < minval(Ecm) .and. e > maxval(Ecm)) stop "STOP: Interp out of range"
    call interp_phase_shifts (Ecm, S31, e, iS31, istat(2))
    call interp_phase_shifts (Ecm, P31, e, iP31, istat(5))
    call interp_phase_shifts (Ecm, P33, e, iP33, istat(6))
    
    ! WARNING: disable GSL error handler
    std = fgsl_set_error_handler_off()
    fgsl_sf_stat = fgsl_sf_bessel_kc0_e(e/temp, besk0)
    if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk0)
    
    fgsl_sf_stat = fgsl_sf_bessel_kcn_e(2,e/temp, besk2)
    if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk2)
    
    ! return back standard error handler
    off = fgsl_set_error_handler(std)
    
    dnp_func = e*e*e * (besk0%val+besk2%val) * (iS31 + 3.0_dp * (iP31+iP33))
    !write(124,*) e, iS31, iP31, iP33
  end function dnp_func
  
  ! proton-pion derivative second virial integrand
  function dpp_func(e, params) bind(c)
    real(dp), value :: e
    type(c_ptr), value :: params
    real(dp) :: dpp_func
    real(dp) :: iS11, iS31, iP11, iP13, iP31, iP33
    integer, dimension(6) :: istat
    integer(fgsl_int) :: fgsl_sf_stat
    type(fgsl_sf_result) :: besk0, besk2
    if (e < minval(Ecm) .and. e > maxval(Ecm)) stop "STOP: Interp out of range"
    call interp_phase_shifts (Ecm, S11, e, iS11, istat(1))
    call interp_phase_shifts (Ecm, S31, e, iS31, istat(2))
    call interp_phase_shifts (Ecm, P11, e, iP11, istat(3))
    call interp_phase_shifts (Ecm, P13, e, iP13, istat(4))
    call interp_phase_shifts (Ecm, P31, e, iP31, istat(5))
    call interp_phase_shifts (Ecm, P33, e, iP33, istat(6))

    ! WARNING: disable GSL error handler
    std = fgsl_set_error_handler_off()
    fgsl_sf_stat = fgsl_sf_bessel_kc0_e(e/temp, besk0)
    if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk0)
    
    fgsl_sf_stat = fgsl_sf_bessel_kcn_e(2,e/temp, besk2)
    if (fgsl_sf_stat /= 0) call fgsl_sf_error (fgsl_sf_stat, besk2)
    
    ! return back standard error handler
    off = fgsl_set_error_handler(std)
    
    dpp_func = e*e*e * (besk0%val+besk2%val) * (iS11+iS31 + 3.0_dp * (iP11+iP13+iP31+iP33))
    !write(125,*) e, iS11, iS31, iP11, iP13, iP31, iP33
  end function dpp_func
  
end subroutine second_virial_coef
