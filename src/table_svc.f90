program table_svc
  use,intrinsic::iso_fortran_env,dp=>real64
  implicit none
  real(dp) :: b2npi, b2ppi, db2npi, db2ppi
  real(dp) :: temp_min, temp_max
  real(dp), allocatable :: tarray(:)
  integer :: i, ntemp
  character(len=*), parameter :: outdir = 'out', outfile = 'svc_vs_temp.dat'
  
  interface
    subroutine second_virial_coef (temp, b2npi, b2ppi, db2npi, db2ppi)
      use,intrinsic::iso_c_binding,dp=>c_double
      real(dp), intent(in) :: temp
      real(dp), intent(out) :: b2npi, b2ppi, db2npi, db2ppi
    end subroutine second_virial_coef
  end interface

  ntemp = 1000
  temp_min = 0.0_dp
  temp_max = 300.0_dp
  allocate(tarray(ntemp))
  tarray   = [ (temp_min+(i-1)*(temp_max-temp_min)/(ntemp-1), i=1, ntemp) ]

!  test: block
!    real(dp) :: temp
!    do
!      print'(":: Temperature: ")'
!      read(input_unit,*) temp
!      call second_virial_coef(temp, b2npi, b2ppi, db2npi, db2ppi)
!      write(*,*) temp, b2npi, b2ppi, db2npi, db2ppi
!    end do
!  end block test  

  save_data: block
    integer :: iounit
    open(newunit=iounit, file=outdir//'/'//outfile)
    write(iounit,'("# T (MeV)",5x,"b2npi",5x,"b2ppi",5x,"db2npi",5x,"db2ppi")')
    do i=1, ntemp
       call second_virial_coef(tarray(i), b2npi, b2ppi, db2npi, db2ppi)
       write(iounit,*) tarray(i), b2npi, b2ppi, db2npi, db2ppi
       write(*,*) tarray(i), b2npi, b2ppi, db2npi, db2ppi
    end do
  end block save_data

end program table_svc
