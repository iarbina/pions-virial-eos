subroutine print_results (rho, temp, ye_in, ymu_in,  &
    b2, nden, pres, ener, chem, sent, flags)
  use,intrinsic::iso_fortran_env,dp=>real64
  use defs_mod
  use constants
  implicit none
  real(dp), intent(in) :: rho, temp, ye_in, ymu_in, b2(4)
  type(particle_density)   , intent(in) :: nden
  type(generic_all)        , intent(in) :: pres, ener, sent
  type(chemical_potentials), intent(in) :: chem
  type(flag_types)         , intent(in) :: flags
  real(dp) :: b2npi, b2ppi, db2npi, db2ppi
  real(dp) :: zn, zp, zpi, sbar, nb
  logical :: pions, muons, pi_interact, betaequi_tf, output_file, external_call
  logical :: fstat
  integer :: iounit
  
  namelist /control/ muons, pions, pi_interact, betaequi_tf, output_file, external_call

  ! open and read 'control' namelist
  open(newunit=iounit, file='inp/input_list.nml', status='old', &
       action='read', form='formatted', access='sequential')
  read(iounit, nml=control)
  close(iounit)
  
  nb = nden%nb
  b2npi = b2(1); b2ppi = b2(2); db2npi = b2(3); db2ppi = b2(4)
  
  ! fugacities
  zn  = exp((chem%n-mnmev)/temp)
  zp  = exp((chem%p-mpmev)/temp)
  zpi = exp((chem%pi-mpimev)/temp)

     
  write(output_unit,'(a,"Second virial coefficient at temperature ",f5.1," MeV")') new_line('a'), temp
  write(output_unit,'(" > neutron-pion: ",es10.3)') b2npi
  write(output_unit,'(" > proton-pion : ",es10.3)') b2ppi
  write(output_unit,'(" > deriv n-pi  : ",es10.3)') db2npi
  write(output_unit,'(" > deriv p-pi  : ",es10.3)') db2ppi
  write(output_unit,*)

  write(output_unit,*)
  write(output_unit,'(" Results for rho: ",(1x,es11.4,1x),"g/cm^3")') rho
  write(output_unit,'("              nb: ",(1x,es11.4,1x),"fm^-3")') nb
  write(output_unit,'("               T: ",(1x,es11.4,1x),"MeV")') temp
  write(output_unit,'("              ye: ",(1x,es11.4,1x))') ye_in
  write(output_unit,'("             ymu: ",(1x,es11.4,1x))') ymu_in
  write(output_unit,*) repeat('-',50)
  write(output_unit,*)
  write(output_unit,'(" Chemical potentials: μhat = μn - μp = μπ")')
  write(output_unit,*) repeat('-',50)
  write(output_unit,'(" μn, μp, μhat: ",3(1x,es11.4))')  &
  chem%n, chem%p, chem%hat
  write(output_unit,'(" μe, μμ-, μπ-: ",3(1x,es11.4))') chem%e, chem%mu, chem%pi
  write(output_unit,*)
  write(output_unit,'(" Abundances")')
  write(output_unit,*) repeat('-',50)
  write(output_unit,'(" Yn,  Yp,   δ: ",3(1x,es11.4))') nden%n/nb, nden%p/nb, (nden%n-nden%p)/nb
  write(output_unit,'(" Ye, Ye-, Ye+: ",3(1x,es11.4))') nden%e%net/nb, nden%e%par/nb, nden%e%ant/nb
  write(output_unit,'(" Yμ, Yμ-, Yμ+: ",3(1x,es11.4))') nden%mu%net/nb, nden%mu%par/nb, nden%mu%ant/nb
  if (pi_interact) &
  write(output_unit,'(" Yπ, Yπb, Yπi: ",3(1x,es11.4))') nden%pi%tot/nb, nden%pi%pim/nb, (nden%pi%npi+nden%pi%ppi)/nb
  if (.not.pi_interact) &
  write(output_unit,'(" Yπ-,Yπ+, Yπ0: ",3(1x,es11.4))') nden%bpi%pim/nb, nden%bpi%pip/nb, nden%bpi%pi0/nb
  if (.not.pi_interact) &
  write(output_unit,'(" Yπnet, Yπcon: ",2(1x,es11.4))') nden%bpi%net/nb, nden%bpi%pic/nb
  write(output_unit,*)
  write(output_unit,'(15x,"Yp         = Ye + Yμ + Yπ [+ Yπc]")')
  if (pi_interact) &
  write(output_unit,'(6x,g0,1x,"=",1x,g0)') nden%p/nb, (nden%e%net+nden%mu%net+nden%pi%tot)/nb 
  if (.not.pi_interact) &
  write(output_unit,'(6x,g0,1x,"=",1x,g0)') nden%p/nb, (nden%e%net+nden%mu%net+nden%bpi%net+nden%bpi%pic)/nb 
  write(output_unit,*)
  write(output_unit,'(" Pressures (MeV/fm^3)")')
  write(output_unit,*) repeat('-',50)
  write(output_unit,'(" Pbar, Prad  : ",2(1x,es11.4))') pres%bar, pres%rad
  write(output_unit,'(" Pe, Pe-, Pe+: ",3(1x,es11.4))') pres%e%net, pres%e%par, pres%e%ant
  write(output_unit,'(" Pμ, Pμ-, Pμ+: ",3(1x,es11.4))') pres%mu%net, pres%mu%par, pres%mu%ant
  if (pions .and. pi_interact) then
     write(output_unit,'(" Pπt,Pπb, Pπi: ",3(1x,es11.4))') pres%pi%tot, pres%pi%pim, pres%pi%npi+pres%pi%ppi 
     write(output_unit,'(" Ptot        : ",  1x,es11.4)') pres%bar + pres%e%net + pres%mu%net + pres%pi%tot
  else if (pions .and. .not.pi_interact) then
     write(output_unit,'(" Pπ-,Pπ+, Pπ0: ",3(1x,es11.4))') pres%bpi%pim, pres%bpi%pip, pres%bpi%pi0 
     write(output_unit,'(" Pπnet, Pπcon: ",2(1x,es11.4))') pres%bpi%net, pres%bpi%pic
     write(output_unit,'(" Ptot        : ",  1x,es11.4)') pres%bar + pres%e%net + pres%mu%net + pres%bpi%net + pres%bpi%pi0
  else
     write(output_unit,'(" Ptot        : ",  1x,es11.4)') pres%bar + pres%e%net + pres%mu%net
  end if
  write(output_unit,*)
  write(output_unit,'(" Energies (MeV/fm^3)")')
  write(output_unit,*) repeat('-',50)
  write(output_unit,'(" Ubar, Urad  : ",2(1x,es11.4))') ener%bar, ener%rad
  write(output_unit,'(" Ue, Ue-, Ue+: ",3(1x,es11.4))') ener%e%net, ener%e%par, ener%e%ant
  write(output_unit,'(" Uμ, Uμ-, Uμ+: ",3(1x,es11.4))') ener%mu%net, ener%mu%par, ener%mu%ant
  if (pions .and. pi_interact) then
     write(output_unit,'(" Uπt,Uπb, Uπi: ",3(1x,es11.4))') ener%pi%tot, ener%pi%pim, ener%pi%npi+ener%pi%ppi 
     write(output_unit,'(" Utot        : ",  1x,es11.4)') ener%bar + ener%e%net + ener%mu%net + ener%pi%tot
  else if (pions .and. .not.pi_interact) then
     write(output_unit,'(" Uπ-,Uπ+, Uπ0: ",3(1x,es11.4))') ener%bpi%pim, ener%bpi%pip, ener%bpi%pi0 
     write(output_unit,'(" Uπnet, Uπcon: ",2(1x,es11.4))') ener%bpi%net, ener%bpi%pic
     write(output_unit,'(" Utot        : ",  1x,es11.4)') ener%bar + ener%e%net + ener%mu%net + ener%bpi%net + ener%bpi%pi0
  else
     write(output_unit,'(" Utot        : ",  1x,es11.4)') ener%bar + ener%e%net + ener%mu%net
  end if
  write(output_unit,*)
  write(output_unit,'(" Fugacities")')
  write(output_unit,*) repeat('-',50)
  write(output_unit,'("           zn: ", (1x,es11.4))') zn
  write(output_unit,'("           zp: ", (1x,es11.4))') zp
  write(output_unit,'("           zπ: ", (1x,es11.4))') zpi
  write(output_unit,'("=============================END================================")')
  write(output_unit,*)

  if (flags%betae%log_val) &
  write(output_unit,'("> Beta equilibrium error: ",g0)') flags%betae%re_val
  !if (flags%charg%log_val) &
  write(output_unit,'("> Charge neutrality absolute error: ",g0)') flags%charg%re_val
  write(output_unit,'("> Small enough fugacities?: ",g0)') flags%virial%log_val


  block
  real(dp) :: npi, ptot, etot, ppi, epi, npic, npi0, ppi0, epi0
  if (pions .and. pi_interact) then
     npi = nden%pi%tot
     ppi = pres%pi%tot
     epi = ener%pi%tot
     npic = 0.0_dp
     npi0 = 0.0_dp
     ppi0 = 0.0_dp
     epi0 = 0.0_dp
  else if (pions .and. .not.pi_interact) then
     npi  = nden%bpi%net
     ppi  = pres%bpi%net
     ppi0 = pres%bpi%pi0
     epi  = ener%bpi%net
     epi0 = ener%bpi%pi0
     npic = nden%bpi%pic
     npi0 = nden%bpi%pi0
  else
     npi = 0.0_dp
     ppi = 0.0_dp
     epi = 0.0_dp
     npic = 0.0_dp
     npi0 = 0.0_dp
     ppi0 = 0.0_dp
     epi0 = 0.0_dp
  end if

  if (output_file) then
     inquire(file='out/dump.dat', exist=fstat)
     if (fstat) then
        open(1234, file='out/dump.dat', position='append', status='old', action='write')
     else
        open(1234, file='out/dump.dat', action='write')
        write(1234,*) "# rho  T   nb  Yp  Ye  Ymu  Ypi&
          &  mu_p   mu_n  mu_e  mu_mu  mu_pi  Pbar  Pe  Pmu  Ppim&
          &  Pnpi  Pppi  Ubar  Ue  Umu  Upim&
          &  Unpi  Uppi" 
        end if
     write(1234,'(24(1x,es11.4))') rho, temp, nden%nb,    &
       nden%p/nb, nden%e%net/nb, nden%mu%net/nb, nden%pi%tot/nb,  &
       chem%p, chem%n, chem%e, chem%mu, chem%pi,          &
       pres%bar,pres%e%net,pres%mu%net,pres%pi%pim,pres%pi%npi,pres%pi%ppi,  &
       ener%bar,ener%e%net,ener%mu%net,ener%pi%pim,ener%pi%npi,ener%pi%ppi
       !nden%bpi%pic, nden%bpi%pi0,           &
       !pres%bpi%pic, pres%bpi%pi0, ener%bpi%pic, ener%bpi%pi0
       !nden%p/nb, nden%n/nb, nden%e%net/nb, nden%mu%net/nb, nden%pi%tot/nb,  &
       !chem%p, chem%n, chem%e, chem%mu, chem%pi,                             &
       !pres%bar, pres%e%net, pres%mu%net, pres%pi%tot, pres%tot, ener%tot
     close(1234)
  end if
  end block
  
end subroutine print_results
