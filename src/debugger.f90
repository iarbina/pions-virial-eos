program debugger
  use, intrinsic :: iso_fortran_env, dp=>real64
  use fgsl
  use constants
  use defs_mod
  use skyrme_mod
  use bareos_mod
  use boseos_mod
  use root_finder_mod
  use fermi_integrals_mod
  implicit none
  real(dp) :: nb, rho, temp, spres, sedens, np, yp
  real(dp) :: rho_max, rho_min, temp_max, temp_min, yp_min, yp_max
  real(dp), allocatable, dimension(:) :: rhoarray, tarray, yparray
  integer  :: nrho, ntemp, nyp, istat
  integer  :: i, j ,k
  type(particle_density)    :: nden
  type(chemical_potentials) :: chem
  type(nucleons) :: n, eta
  type(nucleons) :: upot, meff
  

  nrho     = 100;      ntemp    = 100;     nyp = 60
  rho_min  = 1.e0_dp; rho_max  = 2.e15_dp
  temp_min = 0.1_dp; temp_max = 200.0_dp
  yp_min   =  0.001_dp; yp_max = 0.999_dp

  allocate( rhoarray(nrho), tarray(ntemp), yparray(nyp) )
  tarray   = [ (temp_min*10.0_dp**((i-1)*(log10(temp_max)-log10(temp_min))/(ntemp-1)), i=1, ntemp) ]
  rhoarray = [ (rho_min *10.0_dp**((i-1)*(log10(rho_max)-log10(rho_min))/(nrho-1)), i=1, nrho) ]
  yparray  = [ (yp_min + (i-1)*(yp_max-yp_min)/(nyp-1), i=1, nyp) ]
  
  do
  print'(":: Enter density and temperature: ")'
  read(input_unit,*) rho, temp, yp
  !do i=1, nrho
  !   rho = rhoarray(i)
  !do j=1, ntemp
  !   temp = tarray(j)
  !do k=1, nyp
     nb = rho/amu * 1.0e-39_dp
     np = yp * nb
     call etabar (nb, temp, np, eta%n, eta%p, istat)
     call skyrme_model (nb, np, temp, eta, meff, upot, spres, sedens)
     
     chem%n = temp * eta%n + mnmev + upot%n
     chem%p = temp * eta%p + mpmev + upot%p
     
     !write(544,*) rho, temp, yparray(k), chem%n-chem%p, chem%n, chem%p
     write(549,'(11(1x,es15.5))') rho, temp, yparray(k), spres, sedens, chem%n, chem%p, meff%n, meff%p, upot%n, upot%p
     !write(*,*) eta%n, eta%p, np
     
     !if (-mpimev < chem%n-chem%p .and. chem%n-chem%p < mpimev ) then
     !   call abundbos (mpimev, temp, chem%n-chem%p,  &
     !      nden%bpi%net, nden%bpi%pim, nden%bpi%pip)
     !   write(545,*) rho, temp, yparray(k), yparray(k) - nden%bpi%net
     !end if
  !end do
  !end do
  !end do
  end do

STOP
  

  block
    use, intrinsic :: iso_c_binding
    real(dp), allocatable :: x(:)
    integer  :: i, n
    real(fgsl_double), allocatable, target :: fpar(:)
    type(c_ptr) :: ptr
    n = 100
    print'(":: Enter density and temperature: ")'
    read(input_unit,*) rho, temp
    nb = rho/amu * 1.0e-39_dp
    x = [ ((i-1)*nb/(n-1), i=1, n) ]
    do i=1, n
       call etabar (nb, temp, x(i), eta%n, eta%p, istat)
       fpar = [nb, temp, mpmev, eta%p]
       ptr = c_loc(fpar)
       write(*,*) x(i), abundbar_binc(x(i), ptr)
    end do
  end block
  stop 'stop'
      
  do
  nucleons_numdens: block
    real(dp), allocatable :: mroot(:)
    
    print'(":: Enter density and temperature: ")'
    read(input_unit,*) rho, temp
    nb = rho/amu * 1.0e-39_dp
    call fgsl_root ([0.00001_dp*nb, 0.99999_dp*nb], abundbar_binc, &
                    [nb, temp, mpmev], mroot, istat)
    
    if (istat == fgsl_einval .or. .not.allocated(mroot)) then
       stop 'ERROR: fsvc_pi_3d_binc: No root found for np'
    else
       n%p = mroot(1)
       n%n = nb - n%p
    end if
    
    if (allocated(mroot)) deallocate(mroot)
  end block nucleons_numdens
  end do
      

end program debugger
