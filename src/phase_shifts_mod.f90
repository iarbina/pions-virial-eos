module phase_shifts_mod
  use,intrinsic::iso_c_binding,dp=>c_double
  use iso_fortran_env, only: output_unit
  use fgsl
  use constants
  implicit none
  
  private
  public :: read_phase_shifts, interp_phase_shifts
  
  ! internal variable
  integer,  parameter :: nfile = 99
  real(dp), dimension(nfile), public :: Ecm, S11, S31, P11, P13, P31, P33
  character(len=*), parameter :: inpath = 'inp'

contains

  ! load phase shifts data
  subroutine read_phase_shifts
    integer :: i
    logical, save :: init = .false.
    if (init) return
    if (.not. init) init = .true.
    ! read phase shifts
    open(unit=1, file=inpath//'/phase_shifts.dat')
    do i=1, nfile
       read(1,*) Ecm(i), S11(i), S31(i), P11(i), P13(i), P31(i), P33(i)
       !write(*,*) Ecm(i), S11(i), S31(i), P11(i), P13(i), P31(i), P33(i)
    end do
    close(unit=1)
  end subroutine read_phase_shifts
  
  ! interpolate phase shifts data
  subroutine interp_phase_shifts (xa, ya, x_in, y_out, istat)
    real(dp), dimension(:), intent(in) :: xa, ya
    real(dp), intent(in)  :: x_in
    real(dp), intent(out) :: y_out
    integer,  intent(out) :: istat
    integer(fgsl_size_t)  :: nmax, itpinit
    type(fgsl_interp_accel) :: acc
    type(fgsl_interp) :: a_interp
    nmax = size(xa)
    acc      = fgsl_interp_accel_alloc()
    a_interp = fgsl_interp_alloc(fgsl_interp_cspline, nmax)
    itpinit  = fgsl_interp_init(a_interp, xa, ya)
    istat    = fgsl_interp_eval_e(a_interp, xa, ya, x_in, acc, y_out)
    call fgsl_interp_free(a_interp)
    call fgsl_interp_accel_free(acc)
    if (istat == fgsl_edom) then
      write(*,*) "fgsl: ERROR: interpolation failed: value out of domain", istat
    else if (istat /= 0) then
       write(*,*) "fgsl: ERROR: interpolation failed", istat
    end if
  end subroutine interp_phase_shifts

end module phase_shifts_mod
