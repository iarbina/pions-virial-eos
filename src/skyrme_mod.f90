module skyrme_mod 
  use, intrinsic::iso_fortran_env, dp=>real64
  use fgsl
  use defs_mod
  use constants
  use fermi_integrals_mod, only: fermi_one_half, fermi_three_halves
  implicit none
  private
  public :: kinn, kinp, dudetan, dudetap
  public :: skyrme_model, skyrme_upot, skyrme_meff
  type(nucleons) :: dudetan, dudetap, dkdeta
  real(dp) :: kinn, kinp
  real(dp), parameter, public :: n0 = 0.16_dp ! fm^-3
  real(dp), parameter :: ha = 0.5_dp
  real(dp), parameter :: es = 0.14416_dp
  real(dp), parameter :: ws = 41.958_dp
  real(dp), parameter, dimension(0:3) :: ts = [-2719.7_dp,    417.64_dp,  -66.687_dp, 15042.0_dp]
  real(dp), parameter, dimension(0:3) :: xs = [0.16154_dp, -0.047986_dp, 0.027170_dp, 0.13611_dp]
  real(dp), parameter :: cs3 = 0.125_dp * (ts(2)*(1.0_dp+2.0_dp*xs(2)) - ts(1)*(1.0_dp+2.0_dp*xs(1)))
  real(dp), parameter :: cs2 = 0.125_dp * (ts(1)*(2.0_dp+xs(1)) + ts(2)*(2.0_dp+xs(2)))
  real(dp), parameter :: cs1 = cs2 + cs3
contains

  subroutine skyrme_model (nb, nprot, temp, eta, smeff, supot, spres, sedens)
    real(dp),       intent(in)  :: nb, nprot, temp
    type(nucleons), intent(in)  :: eta
    real(dp),       intent(out) :: spres, sedens
    type(nucleons), intent(out) :: smeff, supot
    real(dp) :: nneu
    nneu = nb - nprot
    
    ! Skyrme efective masses
    smeff%n = skyrme_meff (mnmev,  nneu, nb)
    smeff%p = skyrme_meff (mpmev, nprot, nb)
    
    ! Skyrme kinetic energy density parameters
    kinn = skyrme_kin (smeff%n, temp, eta%n)
    kinp = skyrme_kin (smeff%p, temp, eta%p)
    dkdeta%n = skyrme_dkin_deta (smeff%n, temp, eta%n)
    dkdeta%p = skyrme_dkin_deta (smeff%p, temp, eta%p)

    sedens   = skyrme_edens(nneu, nprot)
    supot%n  = skyrme_upot (nneu, nprot, 1)
    supot%p  = skyrme_upot (nneu, nprot, 2)
    spres    = skyrme_pres ([nneu, nprot], [smeff%n, smeff%p], [kinn, kinp])
    dudetan%n = dkdeta%n * cs2 
    dudetap%p = dkdeta%p * cs2 
    dudetan%p = dudetan%n
    dudetap%n = dudetap%p
#if (DEBUG > 1)
    write(output_unit,*)
    write(output_unit,'("Skyrme Enuc: ",g0)') sedens/nb
    !write(output_unit,'("Skyrme   Tn: ",g0)') 0.5_dp*kinn/smeff(1)*hbarcfm**2
    !write(output_unit,'("Skyrme   Tp: ",g0)') 0.5_dp*kinp/smeff(2)*hbarcfm**2
    write(output_unit,'("Skyrme    Tn: ",g0)') kinn
    write(output_unit,'("Skyrme    Tp: ",g0)') kinp
    write(output_unit,'("Skyrme    Un: ",g0)') supot%n
    write(output_unit,'("Skyrme    Up: ",g0)') supot%p
    write(output_unit,'("Skyrme Un-Up: ",g0)') supot%n-supot%p
    write(output_unit,'("Skyrme  meff: ",g0)') smeff%n, smeff%p
    write(output_unit,'("Skyrme  pres: ",g0)') spres
#endif
  end subroutine skyrme_model
  
  ! Skyrme energy contribution to the energy
  function skyrme_edens (nn, np)
    real(dp) :: nn, np, skyrme_edens
    real(dp) :: nb, edens
    nb = nn + np
    edens = ha * (kinn/mnmev + kinp/mpmev) * hbarcfm**2
    edens = edens + nb *  (kinn+kinp) * ha*ha * (ts(1)*(1.0_dp+ha*xs(1)) + ts(2)*(1.0_dp+ha*xs(2)))
    edens = edens + (nn*kinn+np*kinp) * ha*ha * (ts(2)*(ha+xs(2)) - ts(1)*(ha+xs(1)))
    edens = edens + ha *  ts(0) * ((1.0_dp+ha*xs(0))*nb*nb - (ha+xs(0))*(nn*nn+np*np))
    edens = edens + ha*ha*ts(3) * ((1.0_dp+ha*xs(3))*nb*nb - (ha+xs(3))*(nn*nn+np*np)) * nb**es/3.0_dp
    skyrme_edens = edens
  end function skyrme_edens
  
  ! Skyrme mean-field potential energy
  function skyrme_upot (nn, np, j)
    integer  :: j
    real(dp) :: nn, np, skyrme_upot
    real(dp) :: nb, upot
    real(dp), dimension(2) :: n, kin
    n = [nn, np]
    kin = [kinn, kinp]
    nb = nn + np
    upot = (kinn+kinp) * ha*ha * (ts(1)*(1.0_dp+ha*xs(1)) + ts(2)*(1.0_dp+ha*xs(2)))
    upot = upot + kin(j) * ha*ha * (ts(2)*(ha+xs(2)) - ts(1)*(ha+xs(1)))
    upot = upot + ts(0) * ((1.0_dp+ha*xs(0))*nb - (ha+xs(0))*n(j))
    upot = upot + ha*ts(3) * ((1.0_dp+ha*xs(3))*nb - (ha+xs(3))*n(j)) * nb**es/3.0_dp
    upot = upot + ha*ha*ts(3) * ((1.0_dp+ha*xs(3))*nb*nb - (ha+xs(3))*(nn*nn+np*np)) * es*nb**(es-1.0_dp)/3.0_dp
    skyrme_upot = upot
  end function skyrme_upot

  ! Skyrme kinetic energy density parameter
  function skyrme_kin (meff, temp, eta)
    real(dp) :: meff, temp, eta, skyrme_kin
    real(dp) :: fact, f32, feta, fbeta
    fact = hbarcfm * hbarcfm
    fact = 2.0_dp*meff*temp/fact
    fact = fact**5
    fact = sqrt(fact)/(2.0_dp*pi*pi)
    !call dfermi (1.5d0, eta, 0.0d0, f32, feta, fbeta)
    f32 = fermi_three_halves(eta)
    skyrme_kin = fact * f32
  end function skyrme_kin 

  ! derivative Skyrme kinetic energy density parameter
  function skyrme_dkin_deta (meff, temp, eta)
    real(dp) :: meff, temp, eta, skyrme_dkin_deta
    real(dp) :: fact, f12, feta, fbeta
    fact = hbarcfm * hbarcfm
    fact = 2.0_dp*meff*temp/fact
    fact = fact**5
    fact = sqrt(fact)/(2.0_dp*pi*pi)
    !call dfermi (1.5d0, eta, 0.0d0, f32, feta, fbeta)
    f12 = fermi_one_half(eta)
    skyrme_dkin_deta = 1.5_dp * fact * f12 
  end function skyrme_dkin_deta

  function skyrme_meff (mass, nnuc, nb)
    real(dp) :: nnuc, nb, mass, smeff, skyrme_meff
    smeff = hbarcfm*hbarcfm
    smeff = 1.0_dp + 2.0_dp*mass*(cs3*nnuc+cs2*nb)/smeff
    skyrme_meff = mass / smeff
  end function skyrme_meff

  function skyrme_pres (n, smeff, kin)
    real(dp) :: n(2), smeff(2), kin(2), skyrme_pres
    real(dp) :: pres, nn, np, nb
    real(dp), parameter :: r53 = 5.0_dp/3.0_dp
    nn = n(1)
    np = n(2)
    nb = nn + np
    pres = (r53/smeff(1) - 1.0_dp/mnmev) * kin(1)
    pres = pres + (r53/smeff(2) - 1.0_dp/mpmev) * kin(2)
    pres = ha*hbarcfm*hbarcfm * pres
    pres = pres + ha *  ts(0) * ((1.0_dp+ha*xs(0))*nb*nb - (ha+xs(0))*(nn*nn+np*np))
    pres = pres + (es+1.0_dp) * ha*ha*ts(3) * ((1.0_dp+ha*xs(3))*nb*nb - (ha+xs(3))*(nn*nn+np*np)) * nb**es/3.0_dp
!    pres = pres + nn*mnmev + np*mpmev
    skyrme_pres = pres
  end function skyrme_pres

end module skyrme_mod
