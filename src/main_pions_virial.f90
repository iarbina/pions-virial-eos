program main_pions_virial
!
!>  ne(:): abundance of electrons
!       0: total = ne(1) + ne(2)
!       1: particle
!       2: anti-particle
!> nmu(:): abundance of electrons
!       0: total = nmu(1) + nmu(2)
!       1: particle
!       2: anti-particle
!>   npie: abundance of pions in excited states
!>   npic: abundance of pions in the condensate
!>   npim: abundance of positive pions = npie + npic
!>   nnpi: abundance of pios due to interaction with neutron
!>   nppi: abundance of pios due to interaction with protons
!
  use,intrinsic::iso_fortran_env,dp=>real64
  use defs_mod
  use constants
  implicit none
  real(dp) :: rho, temp
  real(dp) :: b2(4)
  real(dp) :: ye_in, ymu_in
  type(particle_density)    :: nden
  type(generic_all)         :: pres, ener, sent
  type(chemical_potentials) :: chem
  type(flag_types) :: flags
  logical :: pions, muons, pi_interact, betaequi_tf, output_file, external_call
  integer :: iounit
  character(len=3) :: inp_path = 'inp'

  external pions_virial, print_results
  
  namelist /control/ muons, pions, pi_interact, betaequi_tf, output_file, external_call

  ! open and read 'control' namelist
  open(newunit=iounit, file=inp_path//'/input_list.nml', status='old', &
       action='read', form='formatted', access='sequential')
  read(iounit, nml=control)
  close(iounit)
    

  write(output_unit,'(a," :: Set up -> muons: ",g0,", pions: ",g0,", beta equilibrium: ",g0)')  &
         new_line('a'), muons, pions, betaequi_tf
  write(output_unit,'(1x,a)') repeat('-',52)
  
  ! ask for input data from stdin
  if (betaequi_tf) then
    
    write(output_unit,'(" :: Enter rho (g cm-3), T (MeV), Yp (without commas)",a)') new_line('a')
    read(input_unit,*) rho, temp, ye_in
  
  else if (.not. betaequi_tf) then
    
    if (.not. muons) then
       write(output_unit,'(" :: Enter rho (g cm-3), T (MeV), Ye (without commas)",a)') new_line('a')
       read(input_unit,*) rho, temp, ye_in
       ymu_in = 0.0_dp
    else if (muons) then
       write(output_unit,'(" :: Enter rho (g cm-3), T (MeV), Ye, Ymu (without commas)",a)') new_line('a')
       read(input_unit,*) rho, temp, ye_in, ymu_in
    end if
  end if

  call pions_virial (rho, temp, ye_in, ymu_in,  &
    pions, muons, pi_interact, betaequi_tf,  &
    b2, nden, pres, ener, chem, sent, flags)

  call print_results (rho, temp, ye_in, ymu_in,  &
    b2, nden, pres, ener, chem, sent, flags)

end program main_pions_virial
