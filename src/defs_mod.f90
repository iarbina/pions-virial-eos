module defs_mod
  use, intrinsic :: iso_fortran_env, only: dp => real64
  implicit none
  private :: dp
  
  type nucleons
    real(dp) :: n, p
  end type nucleons
  
  type leptons
    real(dp) :: net, par, ant
  end type leptons

  type bose_pion
    real(dp) :: net, pim, pip, pi0, pic
  end type bose_pion
 
  type inte_pion
    real(dp) :: pim, npi, ppi, tot
  end type inte_pion

  type particle_density
    real(dp) :: n, p, nb
    type(leptons)  :: e, mu
    type(inte_pion) :: pi
    type(bose_pion) :: bpi
  end type particle_density
  
  type generic_all
    real(dp) :: bar, n, p, tot, rad
    type(leptons) :: e, mu
    type(inte_pion) :: pi
    type(bose_pion) :: bpi
  end type generic_all
  
  type chemical_potentials
    real(dp) :: n, p, hat, e, mu, pi
  end type chemical_potentials
  
  type flag_vals
    real(dp):: re_val
    integer :: int_val
    logical :: log_val
  end type flag_vals

  type flag_types
    type(flag_vals) :: betae
    type(flag_vals) :: charg
    type(flag_vals) :: froot
    type(flag_vals) :: virial
  end type flag_types

end module defs_mod
