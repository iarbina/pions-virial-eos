module boseos_mod
  use, intrinsic :: iso_fortran_env, dp=>real64
  use, intrinsic :: iso_c_binding
  use fgsl
  use constants
  implicit none
  private
  public :: abundbos, presbos, enerbos
  real(dp) :: tmass
  real(dp) :: chmass
  real(fgsl_double), parameter :: epsabs = 0.0d0
  real(fgsl_double), parameter :: epsrel = 1.0d-7
  integer(fgsl_size_t), parameter :: limit = 1000_fgsl_size_t
contains
  
  function bdist(x)
    real(dp) :: bdist, x
    if ( x < 36.0d0 ) then
       bdist = 1.0d0/(exp(x)-1.0d0)
    else if ( x < 708.0d0 ) then
       bdist = exp (-x)
    else
       bdist = 0.0d0
    end if
  end function bdist

  function nbose_binc(w, params) bind(c)
    real(c_double), value :: w
    type(c_ptr), value :: params
    real(c_double) :: nbose_binc
    real(dp) :: zzz
    real(dp) :: xxx, xxx1
    real(fgsl_double), pointer :: p(:)
    
    call c_f_pointer(params, p, (/2/))
    tmass  = p(1)
    chmass = p(2)
    zzz  = (w-chmass)/tmass
    xxx1 = bdist(zzz)
    xxx  = sqrt(w*w-1.0d0)*w
    nbose_binc = xxx*xxx1
  end function nbose_binc
  
  function pbose_binc(w, params) bind(c)
    real(c_double), value :: w
    type(c_ptr), value :: params
    real(c_double) :: pbose_binc
    real(dp) :: zzz
    real(dp) :: xxx, xxx1
    real(fgsl_double), pointer :: p(:)
    
    call c_f_pointer(params, p, (/2/))
    tmass  = p(1)
    chmass = p(2)
    zzz  = (w-chmass)/tmass
    xxx1 = bdist(zzz)
    zzz  = w*w-1.0d0
    xxx  = sqrt(zzz)*zzz
    pbose_binc = xxx*xxx1
  end function pbose_binc
  
  function ubose_binc(w, params) bind(c)
    real(c_double), value :: w
    type(c_ptr), value :: params
    real(c_double) :: ubose_binc
    real(dp) :: zzz
    real(dp) :: xxx, xxx1
    real(fgsl_double), pointer :: p(:)
    
    call c_f_pointer(params, p, (/2/))
    tmass  = p(1)
    chmass = p(2)
    zzz  = (w-chmass)/tmass
    xxx1 = bdist(zzz)
    xxx  = sqrt(w*w-1.0d0)*w*w
    ubose_binc = xxx*xxx1
  end function ubose_binc

  subroutine abundbos(mass, tmev, chem, n, npar, nant, mass0, n0)
    ! I/O variables
    real(dp), intent(in)  :: tmev, chem, mass
    real(dp), intent(out) :: n, npar, nant
    real(dp), intent(in), optional :: mass0
    real(dp), intent(out), optional :: n0
    
    real(dp) :: factor
    
    ! FGSL variables
    real(fgsl_double), allocatable, target :: fpar(:)
    real(fgsl_double) :: ra, rda
    integer(fgsl_int) :: status
    type(c_ptr) :: ptr
    type(fgsl_error_handler_t) :: std, off
    type(fgsl_function) :: stdfunc
    type(fgsl_integration_workspace) :: integ_wk
    
    factor = mass/hbarcfm
    factor = factor*factor*factor/(2.0d0*pi*pi)
    ! The integral is performed as a function of the energy in
    ! units of the mass. Hence everything is expressed in units of
    ! the mass
    tmass = tmev/mass
    chmass = chem/mass
    
    ! initialize FGSL integration stuff
    !std = fgsl_set_error_handler_off()
    integ_wk = fgsl_integration_workspace_alloc(limit)
    
    ! calculation for the particle
    fpar = [tmass, chmass]
    ptr  = c_loc(fpar)
    stdfunc = fgsl_function_init(nbose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)

    npar = factor * ra

    ! calculation for the anti-particle
    fpar    = [tmass, -chmass]
    stdfunc = fgsl_function_init(nbose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)

    nant = factor * ra
    
    n = npar - nant
    
    if (present(mass0)) then
       factor  = mass0/hbarcfm
       factor  = factor*factor*factor/(2.0d0*pi*pi)
       tmass   = tmev/mass0
       chmass  = 0.0d0
       fpar    = [tmass, chmass]
       stdfunc = fgsl_function_init(nbose_binc, ptr)
       status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
       n0 = factor * ra
    end if
    
    call fgsl_function_free(stdfunc)
    call fgsl_integration_workspace_free(integ_wk)
    !off = fgsl_set_error_handler(std)

  end subroutine abundbos
  
  ! gets total and individual pressures of bosons
  subroutine presbos (mass, tmev, chem, Ptot, Ppar, Pant, mass0, P0)
    ! I/O variables
    real(dp), intent(in)  :: tmev, chem, mass
    real(dp), intent(out) :: Ptot, Ppar, Pant
    real(dp), intent(in),  optional :: mass0
    real(dp), intent(out), optional :: P0
    
    real(dp) :: factor

    ! FGSL variables
    real(fgsl_double), allocatable, target :: fpar(:)
    real(fgsl_double) :: ra, rda
    integer(fgsl_int) :: status
    type(c_ptr) :: ptr
    !type(fgsl_error_handler_t) :: std, off
    type(fgsl_function) :: stdfunc
    type(fgsl_integration_workspace) :: integ_wk
   
    factor = mass/hbarcfm
    factor = mass * factor*factor*factor / (6.0d0*pi*pi)
    ! The integral is performed as a function of the energy in
    ! units of the mass. Hence everything is expressed in units of
    ! the mass
    tmass = tmev/mass
    chmass = chem/mass
    
    ! initialize FGSL integration stuff
    !std = fgsl_set_error_handler_off()
    integ_wk = fgsl_integration_workspace_alloc(limit)
  
    ! calculation for the particle
    fpar = [tmass, chmass]
    ptr  = c_loc(fpar)
    stdfunc = fgsl_function_init(pbose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
    
    Ppar = factor * ra
    
    ! calculation for the anti-particle
    fpar    = [tmass, -chmass]
    stdfunc = fgsl_function_init(pbose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
    
    Pant = factor * ra 
    
    Ptot = Ppar + Pant
  
    if (present(mass0)) then
       factor = mass0 /hbarcfm
       factor = mass0 * factor*factor*factor / (6.0d0*pi*pi)
       tmass = tmev/mass0
       chmass  = 0.0d0
       fpar    = [tmass, chmass]
       stdfunc = fgsl_function_init(pbose_binc, ptr)
       status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
       P0 = factor * ra
    end if
    
    call fgsl_function_free(stdfunc)
    call fgsl_integration_workspace_free(integ_wk)
    !off = fgsl_set_error_handler(std)
  
  end subroutine presbos
  
  ! gets total and individual internal energies of bosons
  subroutine enerbos (mass, tmev, chem, Utot, Upar, Uant, mass0, U0)
    ! I/O variable
    real(dp), intent(in)  :: tmev, chem, mass
    real(dp), intent(out) :: Utot, Upar, Uant
    real(dp), intent(in), optional :: mass0
    real(dp), intent(out), optional :: U0
    
    real(dp) :: factor

    ! FGSL variables
    real(fgsl_double), allocatable, target :: fpar(:)
    real(fgsl_double) :: ra, rda
    integer(fgsl_int) :: status
    type(c_ptr) :: ptr
    !type(fgsl_error_handler_t) :: std, off
    type(fgsl_function) :: stdfunc
    type(fgsl_integration_workspace) :: integ_wk
   
    factor = mass/hbarcfm
    factor = mass * factor*factor*factor / (2.0d0*pi*pi)
    
    ! The integral is performed as a function of the energy in
    ! units of the mass. Hence everything is expressed in units of
    ! the mass
    tmass = tmev/mass
    chmass = chem/mass
    
    ! initialize FGSL integration stuff
    !std = fgsl_set_error_handler_off()
    integ_wk = fgsl_integration_workspace_alloc(limit)
  
    ! calculation for the particle
    fpar = [tmass, chmass]
    ptr  = c_loc(fpar)
    stdfunc = fgsl_function_init(ubose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
  
    Upar = factor * ra 
  
    ! calculation for the anti-particle
    fpar    = [tmass, -chmass]
    stdfunc = fgsl_function_init(ubose_binc, ptr)
    status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
    
    Uant = factor * ra
    
    Utot = Upar + Uant
  
    if (present(mass0)) then
       factor = mass0/hbarcfm
       factor = mass0 * factor*factor*factor / (2.0d0*pi*pi)
       tmass = tmev/mass0
       chmass = 0.0d0
       fpar    = [tmass, chmass]
       stdfunc = fgsl_function_init(ubose_binc, ptr)
       status  = fgsl_integration_qagiu(stdfunc, 1.0_dp, epsabs, epsrel, limit, integ_wk, ra, rda)
       U0 = factor * ra
       !Utot = Utot + U0
    end if
  
    call fgsl_function_free(stdfunc)
    call fgsl_integration_workspace_free(integ_wk)
    !off = fgsl_set_error_handler(std)
  
  end subroutine enerbos

end module boseos_mod
