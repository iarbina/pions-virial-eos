# Pions in Hot Dense Matter
## Description
Implementation of an EOS with interacting pions through the second virial
coefficient following the work _B. Fore, S. Reddy, "Pions in hot dense matter and their astrophysical implications", Phys. Rev. C 101, 035809 (2020)_.
It also adds the possibility of including muons.
## Usage
1. Yo need to have properly installed `fgsl` (the Fortran interface to GNU Scientific Library). _Properly_ means that 
```bash
pkg-config --libs fgsl gsl
pkg-config --cflags fgsl
``` 
would return the required paths to the library.

2. Compile 
````bash
make main_pions_virial
````
produces binary `main_pions_virial`.
````bash
make table_pions_virial
````
produces binary `table_pions_virial`.

## Content
* `inp/`: input files
  * input_list.nml:
    * `control`: control parameters to whether include muons and/or pions,
       beta-equilibrium, etc. 
    * `table`: parameters to shape EOS table
  * phase_shifts.dat: phase shifts to compute the second virial coefficients
* `obj/`: object files (at compilation time)
* `out/`: out files (if at all)
* `proces/`: original data and scripts to extract the phase shifts from the
  figures
  * epstoedit: converts .eps figures to either .fig firmat or gnuplot .gp
  * organanize2.py: reads .gp files and extracts the phase shifts
* `src/`: source files
  + `main_pions_virial.f90` (main program): reads input namelist `control` and input values to call `pions_virial.f90` and print results.
  + `table_pions_virial.f90` (program): eases the creation of a table, taking shape parameters from input list `table`
     (at the moment only 3D EOS is implemented, TODO: 4D EOS with muons)
  + `pions_virial.f90` (subroutine): main subroutine that orchestrates the calculation: takes the input parameters and configuration and outputs computed quantities stores as derived types
  + `defs_mod.f90` (module): contains user defined derives types
  + `skyrme_mod.f90` (module): contains routines to obtain quantities for a Skryme interaction, e.g. baryon energy density, effective mass, potential, etc.
  + `second_virial_coef.f90` (subroutine): computes the second Virial coefficient for a given temperature., taking the data form `inp/phase_shifts.dat`
  + `bareos_mod.f90` (module): contains routines to compute baryon quantities: abundance, pressure, energy.
  + `lepeos_mod.f90` (module): contains routines to compute lepton quantities: abundance, pressure, energy.
  + `pieos_mod.f90` (module): contains routines to compute lepton quantities: abundance, pressure (TODO), energy (TODO) and the pion-nucleon Virial interaction terms
  + `root_finder_mod.f90` (module): contains subroutines to find roots of the different equations
  + `constants.f90` (module): physical constants
  + ~~`invermi12.f90`~~ (function): returns inverse Fermi function of order 1/2 (TODO: update tu use SRO EOS implementation of Fukushima routines)
  + `fermi_integrals_mod.f90` (module): SRO EOS arrangement of Fuskushima fountions for Fermi integrals
  + `fermi.f` (subrotuine): return Fermi-Dirac function of any order
  + `print_results.f90` (subroutine): self explanatory

## Warnings
If you give crapy values to the code it will produce crapy outputs. Use at your
own risk.
