# Make pions_virial
#
# Precompiler FFLAGS:
# 	-DDEBUG=$(DEBLEV)  : debug level: DEBLEV = 0 (no debud), 1, 2, ...

#SHELL:=/bin/bash

PNAME=pions_virial

FC=gfortran
DEBLEV=0


FFLAGS=-g -cpp -DDEBUG=$(DEBLEV)
ifeq ($(FC), gfortran)
  FFLAGS+=-fbacktrace -Wall -fimplicit-none -fopenmp
endif
ifeq ($(FC), ifort)
  FFLAGS+=-traceback -warn all -qopenmp
endif

ifdef DEBROOT
  FFLAGS+=-DDEBUG_ROOT
endif

SRC=src
OBJ=obj

ifeq ($(FC), gfortran)
  LIBS=$(shell pkg-config --libs fgsl gsl hdf5) -lhdf5_fortran
  INCL=$(shell pkg-config --cflags fgsl hdf5)
  #LIBS=-L$(HOME)/local/lib/fgsl-gfort-10.2.1 -lfgsl -lgsl -lgslcblas -lm
  #INCL=-I$(HOME)/local/include/gfort-10.2.1/fgsl
  IOBJ=-J$(OBJ)
endif
ifeq ($(FC), ifort)
  #LIBS=-L$(HOME)/local/lib/fgsl-ifort-2021.3.0 -lfgsl -lgsl -lgslcblas -lm
  #INCL=-I$(HOME)/local/include/ifort-2021.3.0/fgsl
  LIBS=$(shell pkg-config --libs fgsl gsl)
  INCL=$(shell pkg-config --cflags fgsl)
  IOBJ=-I$(OBJ) -module $(OBJ)
endif

OBJF=fermi.o 
OBJS=constants.o defs_mod.o fermi_integrals_mod.o\
	 skyrme_mod.o boseos_mod.o \
	 bareos_mod.o pieos_mod.o lepeos_mod.o root_finder_mod.o \
	 phase_shifts_mod.o second_virial_coef.o \
	 print_results.o pions_virial.o

OBJF:=$(addprefix $(OBJ)/, $(OBJF))
OBJS:=$(addprefix $(OBJ)/, $(OBJS))

MAIN=main_pions_virial.f90
EXEC=$(basename $(MAIN))

.PHONY: default
default: $(EXEC)

all: $(EXEC) table_pions_virial

.PHONY: $(EXEC)
$(EXEC): $(OBJF) $(OBJS)
	$(FC) $(FFLAGS) $(INCL) $(IOBJ) $^ $(SRC)/$(MAIN) -o $@ $(LIBS)

.PHONY: table_$(PNAME)
table_pions_virial: $(OBJF) $(OBJS)
	$(FC) $(FFLAGS) $(INCL) $(IOBJ) $^ $(SRC)/table_pions_virial.f90 -o $@ $(LIBS)

.PHONY: table_svc
table_svc: $(OBJF) $(OBJS)
	$(FC) $(FFLAGS) $(INCL) $(IOBJ) $^ $(SRC)/table_svc.f90 -o $@ $(LIBS)

.PHONY: debugger
debugger: $(OBJF) $(OBJS)
	$(FC) $(FFLAGS) $(INCL) $(IOBJ) $^ $(SRC)/debugger.f90 -o $@ $(LIBS)


#$(OBJ)/%.o: $(SRC)/%.f
#    $(FC) $(FFLAGS) $(INCL) $(IOBJ) -c $< -o $@ $(LIBS)

$(OBJ)/%.o: $(SRC)/%.f*
	$(FC) $(FFLAGS) $(INCL) $(IOBJ) -c $< -o $@ $(LIBS)

clean:
	@rm *.mod *.o $(OBJ)/*.mod $(OBJ)/*.o $(OBJ)/*_genmod.f90 $(SRC)/*.o $(SRC)/*.mod $(EXEC) table_pions_virial 2>/dev/null || true
	@echo rm *.mod *.o $(OBJ)/*.mod $(OBJ)/*.o $(OBJ)/*_genmod.f90 $(SRC)/*.o $(SRC)/*.mod $(EXEC) table_pions_virial 

debug:
	@echo 'SRCS:' $(SRCS)
	@echo 'OBJF:' $(OBJF)
	@echo 'OBJS:' $(OBJS)
	@echo 'INCL:' $(INCL)
	@echo 'IOBJ:' $(IOBJ)
	@echo 'pkg-config --libs fgsl gsl  :' $(shell pkg-config --libs fgsl gsl)
	@echo 'pkg-config --cflags fgsl gsl:' $(shell pkg-config --cflags fgsl)
